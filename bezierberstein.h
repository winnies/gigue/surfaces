#ifndef BEZIERBERSTEIN_H
#define BEZIERBERSTEIN_H

#include "curve.h"

/*!
 * \brief Classe représentant les courbes de Bézier.
 *
 * La valeur de la courbe pour un paramètre t donné est calculé grâce aux polynômes de Berstein.
 */
class BezierBerstein: public Curve
{
public:
    BezierBerstein();
    BezierBerstein(const std::vector<Point>& controlPoints);

    Point getValue(float t) const;
    float getStartP() const;
    float getEndP() const;

    const std::vector<Point> &controlPoints() const;
    void set(const std::vector<Point> &controlPoints);
    void setControlPoint(size_t u, const Point& point);

private:
    std::vector<Point> m_controlPoints;
    int m_order;
    std::vector<long long> m_coefs;
};

#endif // BEZIERBERSTEIN_H
