#include "parametrization.h"

std::vector<float> Parametrization::linear(const Curve &curve, size_t count) {
    float start = curve.getStartP();
    float end = curve.getEndP();
    assert(start <= end);

    float step = (end - start) / count;
    std::vector<float> result;
    result.reserve(count);

    for(size_t i = 0; i <= count; ++i) {
        result.push_back(start + i * step);
    }

    return result;
}

Grid<Surface::Param> Parametrization::linear(const Surface &surface, size_t countU, size_t countV) {
    auto start = surface.getStartP(), end = surface.getEndP();

    assert(start.u <= end.u);
    assert(start.v <= end.v);

    float stepU = (end.u - start.u) / countU;
    float stepV = (end.v - start.v) / countV;

    Grid<Surface::Param> grid(countU + 1, countV + 1);

    for(size_t u = 0; u <= countU; ++u) {
        for(size_t v = 0; v <= countV; ++v) {
            grid.get(u, v) = {start.u + u * stepU, start.v + v * stepV};
        }
    }

    return grid;
}

std::vector<Curve::Param> Parametrization::avanceeDeFront(const Curve &curve, float step, float minAngle, float maxAngle) {
    auto start = curve.getStartP();
    auto end = curve.getEndP();
    assert(start <= end && step > 0.f /*&& minAngle < maxAngle*/);

    auto getAngle = [](const Point& p1, const Point& p2) {
        auto v1 = p1.toQVector(), v2 = p2.toQVector();
        return std::acos(QVector3D::dotProduct(v1, v2) / (v1.length() * v2.length()));
    };

    int maxLoop = 10000;

    std::vector<Curve::Param> result;
    result.push_back(start);

    std::cout << "step: " << step << " minAngle: " << minAngle << " maxAngle: " << maxAngle << std::endl;

    for (float t(start); t+step < end && maxLoop > 0; --maxLoop) {
        float next(t + step);
        float angle = std::abs(getAngle(curve.getValue(t), curve.getValue(next)));

        if (angle > maxAngle) {
            step /= 2.f;
            std::cout << "t: " << t << " t + step: " << next << " angle: " << angle << " trop grand" << std::endl;
        } else {
            if (angle < minAngle) {
                step += step / 2.f;
                std::cout << "t: " << t << " t + step: " << next << " angle: " << angle << " trop petit" << std::endl;
            } else {
                result.push_back(t = next);
                std::cout << "t: " << t << " t + step: " << next << " angle: " << angle << " ok" << std::endl;
            }
        }
    }

    result.push_back(end);

    return result;
}
