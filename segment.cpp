#include <cmath>
#include <cassert>

#include "segment.h"

Segment::Segment(const Point &start, const Point &end)
    : points{start, end}
{
}

void Segment::setStart(const Point & p)
{
	points[0] = p;
}

void Segment::setEnd(const Point & p)
{
	points[1] = p;
}

void Segment::setN(size_t r, const Point & p)
{
    points.at(r) = p;
}

const Point& Segment::getN(size_t r) const
{
    return points.at(r);
}

const Point &Segment::getStart() const
{
    return points[0];
}


const Point &Segment::getEnd() const
{
    return points[1];
}

Point Segment::getValue(float p) const
{
    assert(isValid(p));
    float u = ((p - startP) / (endP - startP));
    return getStart() * (1.f - u) + getEnd() * u;
}

float Segment::getStartP() const
{
    return startP;
}

float Segment::getEndP() const
{
    return endP;
}

const std::vector<Point> &Segment::controlPoints() const
{
    return points;
}

void Segment::set(const std::vector<Point> &controlPoints)
{
      assert(controlPoints.size() == 2);
      points = controlPoints;
}

void Segment::setControlPoint(size_t u, const Point &point)
{
    points[u] = point;
}

float Segment::length() const
{
    float res = 0.0f;

    for (size_t i = 0; i < 3; ++i) {
        res += std::pow(getEnd().getN(i) - getStart().getN(i), 2.0f);
    }

    return std::sqrt(res);
}

std::ostream& operator<<(std::ostream& out, const Segment& s)
{
    return out << s.getStart() << " -- " << s.getEnd();
}
