#include "util.h"

std::vector<long long> Util::pascalTriangle(size_t order)
{
    std::vector<long long> result;

    long long coeff = 1;
    for (size_t i = 1; i <= order+1; i++)
    {
        result.push_back(coeff);
        coeff = coeff * (order+1 - i) / i;
    }

    return result;
}
