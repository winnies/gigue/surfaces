#include <cassert>

#include "beziercasteljau.h"
#include "point.h"
#include "segment.h"

BezierCasteljau::BezierCasteljau():
    m_order(0)
{
}

BezierCasteljau::BezierCasteljau(const std::vector<Point> &controlPoints)
{
    set(controlPoints);
}

Point BezierCasteljau::getValue(float t) const
{
    assert(isValid(t));
    std::vector<Point> currentPoints(m_controlPoints);
    std::vector<Point> nextPoints;
    nextPoints.reserve(currentPoints.size() - 1);

    while(currentPoints.size() > 1) {
        nextPoints.clear();

        for(size_t i = 0; i < currentPoints.size() - 1; ++i) {
            nextPoints.push_back(
                        Segment{currentPoints[i], currentPoints[i+1]}.getValue(t));
        }

        std::swap(currentPoints, nextPoints);
    }

    return currentPoints[0];
}

float BezierCasteljau::getStartP() const
{
    return 0.f;
}

float BezierCasteljau::getEndP() const
{
    return 1.f;
}

const std::vector<Point> &BezierCasteljau::controlPoints() const
{
    return m_controlPoints;
}

void BezierCasteljau::set(const std::vector<Point> &controlPoints)
{
    assert(controlPoints.size() >= 2);
    m_controlPoints = controlPoints;
    m_order = m_controlPoints.size() - 1;
}

void BezierCasteljau::setControlPoint(size_t u, const Point &point)
{
    m_controlPoints[u] = point;
}

