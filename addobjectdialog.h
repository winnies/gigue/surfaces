#ifndef ADDOBJECTDIALOG_H
#define ADDOBJECTDIALOG_H

#include <QDialog>

namespace Ui {
class AddObjectDialog;
}

/*!
 * \brief Boîte de dialogue de sélection d'un objet.
 */
class AddObjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddObjectDialog(QWidget *parent = nullptr);
    ~AddObjectDialog();

    /*!
     * \brief Renvoie la catégorie de l'objet sélectionné.
     * \return la catégorie de l'objet sélectionné
     */
    QString getCategorie() const;

    /*!
     * \brief Renvoie le nom de l'objet sélectionné.
     * \return le nom de l'objet sélectionné
     */
    QString getObject() const;

private:
    void categorieChanged(int index);

private:
    Ui::AddObjectDialog *ui;
};

#endif // ADDOBJECTDIALOG_H
