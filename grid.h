#ifndef POINTGRID_H
#define POINTGRID_H

#include <vector>
#include <cassert>
#include <string>
#include <iostream>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Utils/PropertyManager.hh>

/*!
 * \brief Une grille (matrice).
 * \tparam T les éléments composant la grille
 */
template<typename T>
class Grid
{
public:
    Grid()
        : m_linesNb(0), m_colNb(0)
    {}

    Grid(size_t linesNb, size_t colNb)
        : m_linesNb(linesNb), m_colNb(colNb), m_data(linesNb*colNb)
    {}

    Grid(const std::vector<T>& data, size_t colNb)
    {
        assert(data.size() % colNb == 0);
        m_data = data;
        m_linesNb = data.size() / colNb;
        m_colNb = colNb;
    }

    Grid(std::vector<T>&& data, size_t colNb) {
        assert(data.size() % colNb == 0);
        m_data = std::move(data);
        m_linesNb = data.size() / colNb;
        m_colNb = colNb;
    }

    T& get(size_t line, size_t col) {
        assert(line < m_linesNb && col < m_colNb);
        return m_data[line*m_colNb + col];
    }

    const T& get(size_t line, size_t col) const {
        assert(line < m_linesNb && col < m_colNb);
        return m_data[line*m_colNb + col];
    }

    size_t size() const {
        return m_linesNb * m_colNb;
    }

    size_t getLinesNb() const {
        return m_linesNb;
    }

    size_t getColNb() const {
        return m_colNb;
    }

    /*!
     * \brief Créé une nouvelle grille dont les subdivisions (lignes et colonnes) sont de taille constante.
     * \param linesNb nombre de lignes désirées
     * \param colNb nombre de colonnes désirées
     * \param lineLength intervalle des valeurs de T[0]
     * \param colLength intervalle des valeurs de T[1]
     * \return une grille de taille linesNb * colNb
     *
     * T doit implémenter le right operand brace-init-list de taille 3.
     * Retourne une grille régulière d'objets de type T.
     * Les valeurs de T[0] et de T[1] correspondent à linesNb subdivisions de lineLength et colNb subdivisions de colLength respectivement.
     */
    static Grid<T> newLinear(size_t linesNb, size_t colNb, float lineLength, float colLength) {
        Grid<T> g(linesNb, colNb);

        float lineD = lineLength / (g.getLinesNb() - 1), colD = colLength / (g.getColNb() - 1);
        for (size_t line = 0; line < g.getLinesNb(); ++line) {
            for (size_t column = 0; column < g.getColNb(); ++column) {
                g.get(line, column) = {colD * column, lineD * line, 0};
            }
        }
        return g;
    }

    template <typename Mesh>
    /*!
     * \brief Retourne une grille à partir d'un maillage.
     * \param mesh maillage à convertir en grille.
     * \return une grille d'objets T déduite à partir du maillage.
     *
     * T et Mesh::Point doivent implémenter operator[](i) pour i allant de 0 à 2. Ce dernier doit retourner le même type dans les deux cas.
     * Le maillage fourni doit de plus être en forme de grille.
     */
    static Grid<T> fromMesh(const Mesh& mesh) {
        using VH = typename Mesh::VertexHandle;

        VH border1;
        for (auto v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
            if (mesh.valence(*v_it) != 2)
                continue;
            border1 = *v_it;
        }

        if (!border1.is_valid()) {
            return Grid();
        }

        auto hah = mesh.halfedge_handle(border1);
        if (mesh.face_handle(hah).is_valid()) {
            hah = mesh.opposite_halfedge_handle(hah);
            hah = mesh.next_halfedge_handle(hah);
        }

        size_t length1 = 0;
        VH border2;
        do {
            length1 += 1;
            border2 = mesh.to_vertex_handle(hah);
            hah = mesh.next_halfedge_handle(hah);
        } while (mesh.valence(border2) != 2);

        size_t length2 = 0;
        VH border3;
        do {
            length2 += 1;
            border3 = mesh.to_vertex_handle(hah);
            hah = mesh.next_halfedge_handle(hah);
        } while (mesh.valence(border3) != 2);

        Grid<T> grid(length2 + 1, length1 + 1);

        auto setG = [&grid, &mesh](size_t l, size_t c, VH vh) {
            for (short i = 0; i < 3; ++i) {
                grid.get(l, c)[i] = mesh.point(vh)[i];
            }
        };

        setG(0, 0, border3);

        for (size_t i = 1; i < grid.getColNb(); ++i) {
            setG(0, i, mesh.to_vertex_handle(hah));
            hah = mesh.next_halfedge_handle(hah);
        }

        for (size_t line = 1; line < grid.getLinesNb(); line++) {
            if (line % 2 == 0) {
                for (size_t column = 0; column < grid.getColNb() - 1; ++column) {
                    setG(line, column, mesh.to_vertex_handle(hah));

                    hah = mesh.next_halfedge_handle(hah);
                    hah = mesh.next_halfedge_handle(hah);
                    hah = mesh.opposite_halfedge_handle(hah);
                }

                setG(line, grid.getColNb() - 1, mesh.to_vertex_handle(hah));

                hah = mesh.next_halfedge_handle(hah);
            } else {
                for (size_t column = grid.getColNb() - 1; column > 0; --column) {
                    setG(line, column, mesh.to_vertex_handle(hah));

                    hah = mesh.opposite_halfedge_handle(hah);
                    hah = mesh.next_halfedge_handle(hah);
                    hah = mesh.next_halfedge_handle(hah);
                }

                setG(line, 0, mesh.to_vertex_handle(hah));

                hah = mesh.opposite_halfedge_handle(hah);
                hah = mesh.prev_halfedge_handle(hah);
                hah = mesh.opposite_halfedge_handle(hah);
            }
        }

        return grid;
    }

    template <typename Mesh>
    /*!
     * \brief Retourne un maillage correspondant à la grille.
     * \return Un maillage déduit à partir de la grille d'objets T
     *
     * T doit implémenter operator[](i) pour i allant de 0 à 2.
     */
    Mesh toMesh() const {
        Mesh mesh;
        mesh.reserve(getLinesNb() * getColNb(),
                     getColNb() * (getLinesNb() - 1) + getLinesNb() * (getColNb() - 1),
                     (getLinesNb() - 1) * (getColNb() - 1));

        OpenMesh::VPropHandleT<OpenMesh::Vec2ui> coords;
        mesh.add_property(coords, "coords");

        Grid<typename Mesh::VertexHandle> vhs(getLinesNb(), getColNb());

        for (size_t line = 0; line < getLinesNb(); ++line) {
            for (size_t column = 0; column < getColNb(); ++column) {
                auto v = get(line, column);
                auto vh = mesh.add_vertex({v[0], v[1], v[2]});
                vhs.get(line, column) = vh;
                mesh.property(coords, vh) = {line, column};
            }
        }

        for (size_t line = 0; line < getLinesNb() - 1; ++line) {
            for (size_t column = 0; column < getColNb() - 1; ++column) {
                mesh.add_face(vhs.get(line, column), vhs.get(line+1, column),
                              vhs.get(line+1, column+1), vhs.get(line, column+1));
            }
        }

        return mesh;
    }

    /*!
     * \brief Retourne une grille à partir du maillage contenu dans le fichier en paramètre.
     * \param filename nom du fichier contenant le maillage
     * \return une grille à partir du maillage contenu dans le fichier
     */
    static Grid<T> readGrid(const std::string& filename) {
        typedef OpenMesh::PolyMesh_ArrayKernelT<OpenMesh::DefaultTraits> MyMesh;
        MyMesh mesh;

        if (!OpenMesh::IO::read_mesh(mesh, filename))
        {
          std::cerr << "read error\n";
          return Grid();
        }

        return fromMesh<MyMesh>(mesh);
    }

    /*!
     * \brief Créé un fichier contenant le maillage correspondant à la grille.
     * \param filename nom du fichier devant contenir le maillage (résultat)
     * \return vrai si l'écriture s'est bien déroulée
     */
    bool writeGrid(const std::string& filename) const {
        typedef OpenMesh::PolyMesh_ArrayKernelT<OpenMesh::DefaultTraits> MyMesh;
        auto mesh = toMesh<MyMesh>();

        if (!OpenMesh::IO::write_mesh(mesh, filename))
        {
          std::cerr << "write error\n";
          return false;
        }
        return true;
    }

private:
    size_t m_linesNb, m_colNb;
    std::vector<T> m_data;
};

template <typename Vector>
std::ostream& operator<<(std::ostream& os, const Grid<Vector>& grid) {
    for (size_t line = 0; line < grid.getLinesNb(); ++line) {
        for (size_t column = 0; column < grid.getColNb(); ++column) {
            os << grid.get(line, column) << " ";
        }
        os << std::endl;
    }

    return os;
}

#endif // POINTGRID_H
