#ifndef BSPLINE_H
#define BSPLINE_H

#include <vector>
#include "curve.h"
#include "surface.h"

/*!
 * \brief Calcule la valeur d'une spline.
 * \param ts vecteur de noeuds
 * \param i index du point de contrôle à considérer
 * \param n ordre de la spline
 * \param t paramètre de la courbe
 * \return la valeur de la spline selon ts, n, i, n et t
 */
float spline(const std::vector<float> &ts, int i, int n, float t);

/*!
 * \brief Le type de B-Spline voulu.
 */
enum class SplineType {
    Uniform, /*!< les éléments dans le vecteur de noeuds sont séparés par des intervalles identiques */
    Bezier /*!< le vecteur de noeud est constitué uniquement de deux noeuds, 0 et 1, chacun de multiplicité ordre + 1 */
};

/*!
 * \brief Classe représentant les courbes B-Spline.
 *
 * Le type de B-Spline est choisi à l'aide de #SplineType.
 *
 */
class BSplineCurve: public Curve
{
public:
    BSplineCurve();
    BSplineCurve(const std::vector<Point>& controlPoints);
    BSplineCurve(const std::vector<Point> &controlPoints, unsigned long order, SplineType type);

    Point getValue(float t) const;
    float getStartP() const;
    float getEndP() const;

    const std::vector<Point> &controlPoints() const;
    void set(const std::vector<Point> &controlPoints);
    void set(const std::vector<Point> &controlPoints, unsigned long order, SplineType type);
    void setControlPoint(size_t u, const Point& point);

private:
    std::vector<Point> m_controlPoints;
    std::vector<float> tab_t;
    int m_order;
};

/*!
 * \brief Classe représentant les surfaces B-Spline.
 *
 * Le type de B-Spline est choisi à l'aide de #SplineType.
 *
 */
class BSplineSurface : public Surface {
public:
    BSplineSurface();
    BSplineSurface(const Grid<Point>& controlPoints);
    BSplineSurface(const Grid<Point> &controlPoints, unsigned long uOrder, unsigned long vOrder, SplineType type);

    Point getValue(const Param& param) const;

    Param getStartP() const;
    Param getEndP() const;

    const Grid<Point> &controlPoints() const;
    void set(const Grid<Point>& controlPoints);
    void set(const Grid<Point>& controlPoints, unsigned long uOrder, unsigned long vOrder, SplineType type);
    void setControlPoint(size_t u, size_t v, const Point& point);

private:
    Grid<Point> m_controlPoints;
    int m_uOrder, m_vOrder;
    SplineType m_type;
    std::vector<float> m_uTs, m_vTs;
};

#endif // BSPLINE_H
