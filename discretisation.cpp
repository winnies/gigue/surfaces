#include "discretisation.h"

std::vector<Point> Discretize::discretize(const Curve &curve, const std::vector<Curve::Param> &params) {
    std::vector<Point> result;
    result.reserve(params.size());

    for(size_t i = 0; i < params.size(); ++i) {
        result.push_back(curve.getValue(params[i]));
    }

    return result;
}

Grid<Point> Discretize::discretize(const Surface &surface, const Grid<Surface::Param> &params) {
    Grid<Point> grid(params.getLinesNb(), params.getColNb());

    for (size_t line = 0; line < grid.getLinesNb(); ++line) {
        for (size_t column = 0; column < grid.getColNb(); ++column) {
            grid.get(line, column) = surface.getValue(params.get(line, column));
        }
    }

    return grid;
}
