#ifndef SURFACEAGGREGATE_H
#define SURFACEAGGREGATE_H

#include <vector>
#include "surface.h"

namespace SurfaceAggregate {

/*!
 * \brief Teste si deux surfaces présentent une continuité c0 au niveau de l'une de leur extrémités respectives.
 *
 * Toutes les combinaisons d'extrémitées (aka première et dernière ligne / colonne des grilles représentant les polyèdres de contrôle)
 * sont testées afin de savoir si tous les points d'une des extrémités de la première surface coincident avec ceux d'une extrémité
 * de la deuxième.
 *
 * \param s1 première surface à tester
 * \param s2 deuxième surface à tester
 * \return vrai si les 2 surfaces présentent une continuité c0
 */
bool isC0(const Surface& s1, const Surface& s2);

}

#endif // SURFACEAGGREGATE_H
