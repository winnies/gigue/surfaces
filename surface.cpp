#include "surface.h"

bool Surface::isValid(const Surface::Param &param) const
{
    auto s = getStartP(), e = getEndP();
    return param.u >= s.u && param.u <= e.u && param.v >= s.v && param.v <= e.v;
}

std::ostream &operator<<(std::ostream &os, const Surface::Param &p) {
    os << "[" << p.u << "," << p.v << "]";
    return os;
}
