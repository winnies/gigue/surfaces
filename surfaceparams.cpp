#include "surfaceparams.h"
#include "ui_surfaceparams.h"

#include "discretisation.h"
#include "parametrization.h"

#include <QFileDialog>
#include <QMessageBox>

SurfaceParams::SurfaceParams(myOpenGLWidget *view, SurfaceObject *surface, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SurfaceParams),
    view(view),
    object(surface),
    watcher(new QFileSystemWatcher(this))
{
    ui->setupUi(this);

    connect(surface, &SurfaceObject::isShownChanged, this, &SurfaceParams::updateView);
    connect(ui->reloadControlPoints, &QPushButton::clicked, this, &SurfaceParams::reloadControlPoints);
    connect(ui->saveControlPoints, &QPushButton::clicked, this, &SurfaceParams::saveControlPoints);
    connect(ui->autoReloadControlPoints, &QCheckBox::clicked, this, &SurfaceParams::watchControlPointsChanged);
    connect(ui->showControlPoints, &QCheckBox::clicked, this, &SurfaceParams::updateView);
    connect(ui->discretNb, QOverload<int>::of(&QSpinBox::valueChanged), this, &SurfaceParams::updateDiscretization);
    connect(ui->discretDrawMethod, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &SurfaceParams::updateDiscretDrawMethod);
    connect(ui->showPoint, &QGroupBox::clicked, this, &SurfaceParams::updateView);
    connect(ui->pointUSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SurfaceParams::updatePoint);
    connect(ui->pointVSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SurfaceParams::updatePoint);
    connect(ui->pointUSlider, &QSlider::valueChanged, [this](int value) {
        ui->pointUSpinBox->setValue((double)value / ui->pointUSlider->maximum());
    });
    connect(ui->pointVSlider, &QSlider::valueChanged, [this](int value) {
        ui->pointVSpinBox->setValue((double)value / ui->pointVSlider->maximum());
    });
    connect(watcher, &QFileSystemWatcher::fileChanged, this, &SurfaceParams::controlPointsFileChanged);
    connect(ui->modifPoint, &QGroupBox::clicked, this, &SurfaceParams::updateControlPoints);
    connect(ui->modifPoint, &QGroupBox::clicked, this, &SurfaceParams::updateView);
    connect(ui->slider_u, &QSlider::valueChanged, ui->value_u, &QSpinBox::setValue);
    connect(ui->slider_v, &QSlider::valueChanged, ui->value_v, &QSpinBox::setValue);
    connect(ui->value_u, QOverload<int>::of(&QSpinBox::valueChanged), this, &SurfaceParams::changeControlPoint);
    connect(ui->value_v, QOverload<int>::of(&QSpinBox::valueChanged), this, &SurfaceParams::changeControlPoint);
    connect(ui->value_modif_x, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SurfaceParams::translateControlPoint);
    connect(ui->value_modif_y, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SurfaceParams::translateControlPoint);
    connect(ui->value_modif_z, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &SurfaceParams::translateControlPoint);

    ui->controlPointsFilename->setVisible(false);
    ui->reloadControlPoints->setEnabled(false);
    ui->autoReloadControlPoints->setEnabled(false);

    updateControlPoints();
    updateDiscretization();

    std::vector<QRgb> colors;
    colors.push_back(qRgb(255, 0, 0));
    pointDI = view->drawPoints({{0, 0, 0}}, colors);
    updatePoint();
    changeControlPoint();
    updateView();
}

SurfaceParams::~SurfaceParams()
{
    delete ui;

    view->remove(discretizationDI);
    view->remove(controlPointsDI);
    view->remove(pointDI);
    view->repaint();
}

void SurfaceParams::updateView()
{
    view->isShown(discretizationDI) = object->isShown();
    view->isShown(controlPointsDI) = object->isShown() && (ui->showControlPoints->isChecked() || ui->modifPoint->isChecked());
    view->isShown(pointDI) = object->isShown() && ui->showPoint->isChecked();
    view->repaint();
}

void SurfaceParams::updateControlPoints()
{
    const auto& points = object->surface->controlPoints();

    ui->slider_u->setMaximum(points.getLinesNb() - 1);
    ui->slider_v->setMaximum(points.getColNb() - 1);
    ui->value_u->setMaximum(points.getLinesNb() - 1);
    ui->value_v->setMaximum(points.getColNb() - 1);

    using namespace OpenMesh;
    using namespace OpenMesh::Attributes;

    struct MyTraits : public OpenMesh::DefaultTraits
    {
        VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
        FaceAttributes ( OpenMesh::Attributes::Normal );
    };
    typedef OpenMesh::PolyMesh_ArrayKernelT<MyTraits> MyMesh;

    auto mesh = points.toMesh<MyMesh>();
    mesh.update_face_normals();
    mesh.update_vertex_normals();

    OpenMesh::VPropHandleT<OpenMesh::Vec2ui> coords;
    auto ok = mesh.get_property_handle(coords, "coords");
    assert(ok);

    OpenMesh::Vec2ui modifPointCoord{ui->value_u->value(), ui->value_v->value()};

    for (auto v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        const auto& coord = mesh.property(coords, *v_it);
        if (ui->modifPoint->isChecked() && coord == modifPointCoord) {
            mesh.set_color(*v_it, {1, 0, 0});
        } else {
            mesh.set_color(*v_it, {0, 0, 1});
        }
    }

    controlPointsDI = view->drawMesh(mesh, controlPointsDI);
    view->drawMode(controlPointsDI) = DrawMode::Point | DrawMode::Line;
    view->repaint();
}

void SurfaceParams::updateDiscretization()
{
    struct MyTraits : public OpenMesh::DefaultTraits
    {
        typedef OpenMesh::Vec3f Color;
        VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
        FaceAttributes ( OpenMesh::Attributes::Normal );
    };
    typedef OpenMesh::PolyMesh_ArrayKernelT<MyTraits> MyMesh;

    auto discretizedObject = Discretize::discretize(*object->surface, Parametrization::linear(*object->surface, ui->discretNb->value(), ui->discretNb->value()));
    auto mesh = discretizedObject.toMesh<MyMesh>();
    mesh.triangulate();
    mesh.update_face_normals();
    mesh.update_vertex_normals();

    for (auto v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        mesh.set_color(*v_it, {0.85, 0.85, 0.85});
    }

    discretizationDI = view->drawMesh(mesh, discretizationDI);
    updateDiscretDrawMethod();
}

void SurfaceParams::updatePoint()
{
    ui->pointUSlider->setValue(ui->pointUSpinBox->value() * ui->pointUSlider->maximum());
    ui->pointVSlider->setValue(ui->pointVSpinBox->value() * ui->pointVSlider->maximum());

    auto start = object->surface->getStartP(), end = object->surface->getEndP();
    auto p = object->surface->getValue({start.u + (end.u - start.u) * ui->pointUSpinBox->value(),
                                        start.v + (end.v - start.v) * ui->pointVSpinBox->value()});
    auto& m = view->matrix(pointDI);
    m.setToIdentity();
    m.translate(p.toQVector());
    view->repaint();
}

void SurfaceParams::reloadControlPoints(bool showMessage)
{
    auto grid = Grid<Point>::readGrid(controlPointsFileName.toStdString());
    if (grid.size() == 0) {
        if (showMessage) {
            QMessageBox::critical(this, tr("Impossible de recharger"), "Une erreur est survenue."); // TODO revoir
        }
        return;
    }

    object->surface->set(grid);

    updateControlPoints();
    updateDiscretization();
    updatePoint();
    changeControlPoint();
}

void SurfaceParams::saveControlPoints()
{
    auto filename = controlPointsFileName;
    if (filename.isEmpty()) {
        filename = object->name;
    }

    filename = QFileDialog::getSaveFileName(this, tr("Enregistrer les points de contrôles"), filename, tr("3D mesh files(*.obj)"));
    if (filename.isEmpty()) {
        return;
    }

    if (!object->surface->controlPoints().writeGrid(filename.toStdString())) {
        QMessageBox::critical(this, tr("Impossible d'enregistrer"), "Une erreur est survenue."); // TODO revoir
        return;
    }

    setControlPointsFile(filename);
}

void SurfaceParams::watchControlPointsChanged(bool enabled)
{
    if (enabled) {
        watcher->addPath(controlPointsFileName);
    } else {
        watcher->removePath(controlPointsFileName);
    }
}

void SurfaceParams::controlPointsFileChanged()
{
    QFile info(controlPointsFileName);
    if (info.exists()) {
        if (info.size() > 0) {
            reloadControlPoints(false);
        }
    } else {
        setControlPointsFile(QString());
    }
}

void SurfaceParams::updateDiscretDrawMethod()
{
    auto method = ui->discretDrawMethod->currentText();
    if (method == "Filaire") {
        view->drawMode(discretizationDI) = DrawMode::Point | DrawMode::Line;
        view->ligthing(discretizationDI) = false;
        view->flatLigthing(discretizationDI) = false;
    } else if (method == "Faces") {
        view->drawMode(discretizationDI) = DrawMode::Triangle | DrawMode::Line;
        view->ligthing(discretizationDI) = true;
        view->flatLigthing(discretizationDI) = true;
    } else if (method == "Faces lissées") {
        view->drawMode(discretizationDI) = DrawMode::Triangle;
        view->ligthing(discretizationDI) = true;
        view->flatLigthing(discretizationDI) = false;
    }
    view->repaint();
}

void SurfaceParams::setControlPointsFile(const QString &filename)
{
    if (controlPointsFileName == filename) {
        return;
    }

    if (!controlPointsFileName.isEmpty() &&
        ui->autoReloadControlPoints->isChecked()) {
        watcher->removePath(controlPointsFileName);
    }

    auto isOk = !filename.isEmpty();
    ui->controlPointsFilename->setVisible(isOk);
    if (isOk) {
        ui->controlPointsFilename->setText(QFileInfo(filename).fileName());
    }
    ui->reloadControlPoints->setEnabled(isOk);
    ui->autoReloadControlPoints->setEnabled(isOk);
    controlPointsFileName = filename;

    if (isOk && ui->autoReloadControlPoints->isChecked()) {
        watcher->addPath(controlPointsFileName);
    }
}

void SurfaceParams::changeControlPoint()
{
    canTranslateControlPoint = false;

    ui->slider_u->setValue(ui->value_u->value());
    ui->slider_v->setValue(ui->value_v->value());

    const auto& point = object->surface->controlPoints().get(ui->value_u->value(), ui->value_v->value());
    ui->value_modif_x->setValue(point.getX());
    ui->value_modif_y->setValue(point.getY());
    ui->value_modif_z->setValue(point.getZ());

    canTranslateControlPoint = true;

    updateControlPoints();
}

void SurfaceParams::translateControlPoint()
{
    if (!canTranslateControlPoint)  {
        return;
    }

    object->surface->setControlPoint(ui->value_u->value(), ui->value_v->value(), Point(ui->value_modif_x->value(), ui->value_modif_y->value(), ui->value_modif_z->value()));
    updateControlPoints();
    updateDiscretization();
    updatePoint();
}
