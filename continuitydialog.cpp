#include "continuitydialog.h"
#include "ui_continuitydialog.h"
#include "surfaceaggregate.h"

class ObjectProxyModel : public QSortFilterProxyModel
{
public:
    ObjectProxyModel(QObject * parent = nullptr)
        : QSortFilterProxyModel(parent)
    {}

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &) const override {
        auto model = dynamic_cast<ObjectModel*>(sourceModel());
        auto object = &model->get(source_row);
        return dynamic_cast<SurfaceObject*>(object) != nullptr;
    };

    bool filterAcceptsColumn(int, const QModelIndex &) const override {
        return true;
    };
};

ContinuityDialog::ContinuityDialog(ObjectModel* objectmodel, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ContinuityDialog),
    m_objectModel(objectmodel)
{
    ui->setupUi(this);

    m_proxy = new ObjectProxyModel(this);
    m_proxy->setSourceModel(objectmodel);
    ui->leftBox->setModel(m_proxy);
    ui->rightBox->setModel(m_proxy);

    connect(ui->leftBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ContinuityDialog::updateComboBoxes);
    connect(ui->rightBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ContinuityDialog::updateComboBoxes);
    connect(ui->checkButton, &QPushButton::clicked, this, &ContinuityDialog::verifyContinuity);

    updateComboBoxes();
}

ContinuityDialog::~ContinuityDialog()
{
    delete ui;
}

void ContinuityDialog::updateComboBoxes()
{
    ui->result->setText(tr("Surfaces non testées"));
    ui->result->setStyleSheet("");
    bool indicesOk = ui->leftBox->currentIndex() != -1 && ui->rightBox->currentIndex() != -1;
    ui->checkButton->setEnabled(indicesOk);
}

void ContinuityDialog::verifyContinuity()
{
    QModelIndex leftIndex = m_proxy->mapToSource(m_proxy->index(ui->leftBox->currentIndex(), 0));
    QModelIndex rightIndex = m_proxy->mapToSource(m_proxy->index(ui->rightBox->currentIndex(), 0));
    Surface& leftSurface = *dynamic_cast<SurfaceObject&>(m_objectModel->get(leftIndex.row())).surface;
    Surface& rightSurface = *dynamic_cast<SurfaceObject&>(m_objectModel->get(rightIndex.row())).surface;

    if(SurfaceAggregate::isC0(leftSurface, rightSurface)) {
        ui->result->setText(tr("Surfaces continues"));
        ui->result->setStyleSheet("color:green;");
    } else {
        ui->result->setText(tr("Surfaces non continues"));
        ui->result->setStyleSheet("color:red;");
    }
}
