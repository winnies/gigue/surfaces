#include <random>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "discretisation.h"
#include "parametrization.h"
#include "beziercasteljau.h"
#include "bezierberstein.h"
#include "bspline.h"
#include "carreaubezier.h"
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include "addobjectdialog.h"
#include "curveparams.h"
#include "surfaceparams.h"
#include "opencontrolpolyhedrondialog.h"
#include "continuitydialog.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->objects->setModel(m_objectModel = new ObjectModel(this));
    connect(ui->actionTester_C0, &QAction::triggered, this, &MainWindow::openContinuityDialog);
    connect(ui->addObject, &QToolButton::clicked, this, &MainWindow::addObject);
    connect(ui->removeObject, &QToolButton::clicked, this, &MainWindow::removeCurrentObject);
    connect(ui->objectName, &QLineEdit::editingFinished, this, &MainWindow::renameCurrentObject);
    connect(ui->objects, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::showCurrentObject);
    connect(ui->objectShown, &QToolButton::clicked, this, &MainWindow::showChanged);

    showCurrentObject();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openContinuityDialog()
{
    auto dialog = new ContinuityDialog(m_objectModel, this);
    dialog->exec();
    return;
}

void MainWindow::addObject()
{
    auto dialog = new AddObjectDialog(this);
    if (dialog->exec() == QDialog::Rejected) {
        return;
    }

    auto cat = dialog->getCategorie();
    auto obj = dialog->getObject();

    auto addObject = [&](Object * object) {
        object->name = QString("%1 %2 (%3)").arg(cat).arg(obj).arg(nextObjectId++);
        ui->objectWidget->addWidget(object->widget);
        m_objectModel->insert(std::unique_ptr<Object>(object));
    };

    auto addCurveObject = [&](Curve * curve) {
        auto o = new CurveObject;
        o->curve = std::unique_ptr<Curve>(curve);
        o->widget = new CurveParams(ui->openGLWidget, o);
        addObject(o);
        return o;
    };

    auto addSurfaceObject = [&](Surface * surface) {
        auto o = new SurfaceObject;
        o->surface = std::unique_ptr<Surface>(surface);
        o->widget = new SurfaceParams(ui->openGLWidget, o);
        addObject(o);
        return o;
    };

    if (cat == "Courbes") {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(-1.f, 1.f);
        std::vector<Point> testPoints;
        for(int i(0); i < 4; ++i) {
            auto p = Point(dis(gen), dis(gen), dis(gen));
            testPoints.push_back(p);
        }

        if (obj == "Bézier (Casteljau)") {
            addCurveObject(new BezierCasteljau(testPoints));
        } else if (obj == "Bézier (Bernstein)") {
            addCurveObject(new BezierBerstein(testPoints));
        } else if (obj == "B-Spline uniforme (splines de degré 2)") {
            if(testPoints.size() <= 2) {
                QMessageBox::warning(this, tr("Erreur"), tr("Le nombre de points de contrôle doit être strictement supérieur à 2\n"
                                                            "(splines d'ordre 2)"));
                return;
            }
            addCurveObject(new BSplineCurve(testPoints, 2, SplineType::Uniform));
        } else if (obj == "B-Spline uniforme (type Bézier)") {
            addCurveObject(new BSplineCurve(testPoints, 0, SplineType::Bezier));
        }
    } else if (cat == "Surfaces") {
        if (obj == "Bézier (Bernstein)") {
            auto polDialog = new OpenControlPolyhedronDialog(this);
            if (polDialog->exec() == QDialog::Rejected) {
                return;
            }

            for (size_t i = 0; i < polDialog->count(); ++i) {
                auto so = addSurfaceObject(new CarreauBezier(polDialog->getGrids()[i]));
                dynamic_cast<SurfaceParams*>(so->widget.data())->setControlPointsFile(polDialog->getFileNames()[i]);
            }
        } else if (obj == "B-Spline uniforme (splines de degré 2)") {
            auto polDialog = new OpenControlPolyhedronDialog(this);
            if (polDialog->exec() == QDialog::Rejected) {
                return;
            }

            for (size_t i = 0; i < polDialog->count(); ++i) {

                const Grid<Point>& g = polDialog->getGrids()[i];
                if(g.getLinesNb() <= 2 || g.getColNb() <= 2) {
                    QMessageBox::warning(this, tr("Erreur"), tr("Le nombre de points de contrôle doit être strictement supérieur à 2\n"
                                                                "(splines d'ordre 2)"));
                    continue;
                }

                auto so = addSurfaceObject(new BSplineSurface(g, 2, 2, SplineType::Uniform));
                dynamic_cast<SurfaceParams*>(so->widget.data())->setControlPointsFile(polDialog->getFileNames()[i]);
            }
        } else if (obj == "B-Spline uniforme (type Bézier)") {
            auto polDialog = new OpenControlPolyhedronDialog(this);
            if (polDialog->exec() == QDialog::Rejected) {
                return;
            }

            for (size_t i = 0; i < polDialog->count(); ++i) {
                auto so = addSurfaceObject(new BSplineSurface(polDialog->getGrids()[i], 0, 0, SplineType::Bezier));
                dynamic_cast<SurfaceParams*>(so->widget.data())->setControlPointsFile(polDialog->getFileNames()[i]);
            }
        }
    }

    ui->objects->setCurrentIndex(m_objectModel->rowCount()-1);
}

void MainWindow::removeCurrentObject()
{
    m_objectModel->remove(ui->objects->currentIndex());
}

void MainWindow::renameCurrentObject()
{
    m_objectModel->setName(ui->objects->currentIndex(), ui->objectName->text());
}

void MainWindow::showChanged(bool checked)
{
    m_objectModel->get(ui->objects->currentIndex()).setIsShown(checked);
    ui->objectShown->setIcon(QIcon::fromTheme(checked ? "view-visible" : "view-hidden"));
}

void MainWindow::showCurrentObject()
{
    auto index = ui->objects->currentIndex();

    ui->objectShown->setEnabled(index != -1);
    ui->objectName->setEnabled(index != -1);
    ui->removeObject->setEnabled(index != -1);

    if (index == -1) {
        ui->objectName->setText("");
    } else {
        bool isShown = m_objectModel->get(index).isShown();
        ui->objectShown->setChecked(isShown);
        ui->objectShown->setIcon(QIcon::fromTheme(isShown ? "view-visible" : "view-hidden"));
        ui->objectName->setText(m_objectModel->getName(index));
        ui->objectWidget->setCurrentWidget(m_objectModel->get(index).widget);
    }
}
