#ifndef SURFACE_H
#define SURFACE_H

#include "point.h"
#include "grid.h"

/*!
 * \brief Classe abstraite représentant les surfaces paramétriques.
 */
class Surface
{
public:
    /*!
     * \brief Paramètre d'une surface
     */
    struct Param {
        float u, v;

        Param() : u(0), v(0) {}

        Param(float u, float v)
            : u(u), v(v) {}

        Param(double u, double v)
            : u(u), v(v) {}
    };

    virtual ~Surface() = default;

    /*!
     * \brief Retourne la valeur de la surface paramétrique pour un paramètre param donné.
     * \param param paramètre de la surface
     * \return la valeur de la surface pour param donné
     */
    virtual Point getValue(const Param& param) const = 0;

    /*!
     * \brief Retourne la borne inférieure du domaine (intervalle des valeurs possibles) du paramètre.
     * \return la borne inférieure du domaine du paramètre
     */
    virtual Param getStartP() const = 0;

    /*!
     * \brief Retourne la borne supérieure du domaine (intervalle des valeurs possibles) du paramètre.
     * \return la borne supérieure du domaine du paramètre
     */
    virtual Param getEndP() const = 0;

    /*!
     * \brief Retourne l'ensemble des points de contrôle de la surface représentant son polyèdre de contrôle.
     * \return l'ensemble des points de contrôle de la surface
     */
    virtual const Grid<Point> &controlPoints() const = 0;

    /*!
     * \brief Attribue l'ensemble des points de contrôle de la surface représentant son polyèdre de contrôle.
     * \param controlPoints l'ensemble des points de contrôle de la surface
     */
    virtual void set(const Grid<Point>& controlPoints) = 0;

    /*!
     * \brief Modifie un point de contrôle de la surface.
     * \param u le paramètre u
     * \param v le paramètre v
     * \param point le nouveau point
     */
    virtual void setControlPoint(size_t u, size_t v, const Point& point) = 0;

    /*!
     * \brief Vérifie si le paramètre est bien défini dans le domaine.
     * \param param paramètre à vérifier
     * \return vrai si le paramètre est compris entre la borne inférieure et la borne supérieure
     */
    bool isValid(const Param& param) const;
};

std::ostream& operator<<(std::ostream& os, const Surface::Param& p);
#endif // SURFACE_H
