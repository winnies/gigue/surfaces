#include "objectmodel.h"

ObjectModel::ObjectModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int ObjectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_objects.size();
}

QVariant ObjectModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto& o = m_objects[index.row()];
    switch (role) {
    case Qt::DisplayRole:
        return o->name;
    default:
        return QVariant();
    }
}

const QString &ObjectModel::getName(int index) const
{
    return m_objects[index]->name;
}

void ObjectModel::setName(int index, const QString &name)
{
    m_objects[index]->name = name;
    auto i = createIndex(index, 0);
    emit dataChanged(i, i, {Qt::DisplayRole});
}

const Object &ObjectModel::get(int index) const
{
    return *m_objects[index];
}

Object &ObjectModel::get(int index)
{
    return *m_objects[index];
}

void ObjectModel::insert(ObjectPtr object)
{
    emit beginInsertRows(QModelIndex(), m_objects.size(), m_objects.size());
    m_objects.push_back(std::move(object));
    emit endInsertRows();
}

void ObjectModel::remove(int index)
{
    emit beginRemoveRows(QModelIndex(), index, index);
    m_objects.erase(m_objects.begin() + index);
    emit endRemoveRows();
}
