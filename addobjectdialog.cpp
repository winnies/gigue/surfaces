#include "addobjectdialog.h"
#include "ui_addobjectdialog.h"

AddObjectDialog::AddObjectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddObjectDialog)
{
    ui->setupUi(this);

    connect(ui->categories, &QListWidget::currentRowChanged, this, &AddObjectDialog::categorieChanged);
    connect(ui->curveObjects, &QListWidget::doubleClicked, this, &AddObjectDialog::accept);
    connect(ui->surfaceObjects, &QListWidget::doubleClicked, this, &AddObjectDialog::accept);
    connect(ui->cancel, &QPushButton::clicked, this, &AddObjectDialog::reject);
    connect(ui->add, &QPushButton::clicked, this, &AddObjectDialog::accept);

    ui->categories->setCurrentRow(0);
    ui->curveObjects->setCurrentRow(0);
    ui->surfaceObjects->setCurrentRow(0);
}

AddObjectDialog::~AddObjectDialog()
{
    delete ui;
}

QString AddObjectDialog::getCategorie() const
{
    return ui->categories->currentItem()->data(Qt::DisplayRole).toString();
}

QString AddObjectDialog::getObject() const
{
    auto cat = getCategorie();
    if (cat == "Courbes") {
        return ui->curveObjects->currentItem()->data(Qt::DisplayRole).toString();
    } else if (cat == "Surfaces") {
        return ui->surfaceObjects->currentItem()->data(Qt::DisplayRole).toString();
    }
    return "";
}

void AddObjectDialog::categorieChanged(int index)
{
    ui->objectsCat->setCurrentIndex(index == -1 ? 0 : index);
}
