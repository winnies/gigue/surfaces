#ifndef DISCRETISATION_H
#define DISCRETISATION_H

#include <vector>

#include "point.h"
#include "curve.h"
#include "surface.h"
#include "grid.h"

/*!
 * \brief Méthodes de discrétisation.
 * \sa Parametrization
 */
namespace Discretize {

/*!
 * \brief Retourne un ensemble de points correspondant à la discrétisation de la courbe selon une paramétrisation donnée.
 * \param curve courbe à discrétiser
 * \param params l'ensemble des paramètres
 * \return une discrétisation de la courbe selon l'ensemble des paramètres donnés
 */
std::vector<Point> discretize(const Curve& curve, const std::vector<Curve::Param>& params);

/*!
 * \brief Retourne un ensemble de points correspondant à la discrétisation de la surface selon une paramétrisation donnée.
 * \param surface surface à discrétiser
 * \param params l'ensemble des paramètres
 * \return une discrétisation de la courbe selon l'ensemble des paramètres donnés
 */
Grid<Point> discretize(const Surface& surface, const Grid<Surface::Param>& params);

}

#endif // DISCRETISATION_H
