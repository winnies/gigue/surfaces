#include <cassert>
#include <cmath>

#include "bezierberstein.h"
#include "point.h"
#include "segment.h"
#include "util.h"

BezierBerstein::BezierBerstein():
    m_order(0)
{
}

BezierBerstein::BezierBerstein(const std::vector<Point> &controlPoints)
{
    set(controlPoints);
}

Point BezierBerstein::getValue(float t) const
{
    Point result;
    assert(isValid(t));

    for(int i(0); i <= m_order; i++) {
        result += m_controlPoints[i] * m_coefs[i] * std::pow((1 - t), m_order - i) * std::pow(t, i);
    }

    return result;
}

float BezierBerstein::getStartP() const
{
    return 0.f;
}

float BezierBerstein::getEndP() const
{
    return 1.f;
}

const std::vector<Point> &BezierBerstein::controlPoints() const
{
    return m_controlPoints;
}

void BezierBerstein::set(const std::vector<Point> &controlPoints)
{
    assert(controlPoints.size() >= 2);
    m_controlPoints = controlPoints;
    m_order = m_controlPoints.size() - 1;
    m_coefs = Util::pascalTriangle(m_order);
}

void BezierBerstein::setControlPoint(size_t u, const Point &point)
{
    m_controlPoints[u] = point;
}
