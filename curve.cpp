#include "curve.h"

bool Curve::isValid(Param param) const {
    return param >= getStartP() && param <= getEndP();
}
