#ifndef CARREAUBEZIER_H
#define CARREAUBEZIER_H

#include "surface.h"

/*!
 * \brief Classe représentent les carreaux de Bézier.
 */
class CarreauBezier : public Surface
{
public:
    CarreauBezier();
    CarreauBezier(const Grid<Point>& controlPoints);

    Point getValue(const Param& param) const;

    Param getStartP() const;
    Param getEndP() const;

    const Grid<Point> &controlPoints() const;
    void set(const Grid<Point>& controlPoints);
    void setControlPoint(size_t u, size_t v, const Point& point);

private:
    Grid<Point> m_controlPoints;
    int m_uOrder;
    int m_vOrder;
    std::vector<long long> m_uCoefs;
    std::vector<long long> m_vCoefs;
};

#endif // CARREAUBEZIER_H
