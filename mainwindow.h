#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "objectmodel.h"

namespace Ui {
class MainWindow;
}

/*!
 * \brief La fenêtre principale de l'application.
 */
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private:
    void addObject();
    void openContinuityDialog();
    void removeCurrentObject();
    void renameCurrentObject();
    void showChanged(bool checked);
    void showCurrentObject();

private:
	Ui::MainWindow *ui;

    size_t nextObjectId = 1;
    ObjectModel* m_objectModel;
};

#endif // MAINWINDOW_H
