#ifndef BEZIERCASTELJAU_H
#define BEZIERCASTELJAU_H

#include <vector>
#include "curve.h"

/*!
 * \brief Classe représentant les courbes de Bézier.
 *
 * La valeur de la courbe pour un paramètre t donné est calculé grâce à l'algorithme de Casteljau.
 */
class BezierCasteljau: public Curve
{
public:
    BezierCasteljau();
    BezierCasteljau(const std::vector<Point>& controlPoints);

    Point getValue(float t) const;
    float getStartP() const;
    float getEndP() const;

    const std::vector<Point> &controlPoints() const;
    void set(const std::vector<Point> &controlPoints);
    void setControlPoint(size_t u, const Point& point);

private:
    std::vector<Point> m_controlPoints;
    int m_order;
};

#endif // BEZIERCASTELJAU_H
