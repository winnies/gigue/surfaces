﻿# Projet : Courbes et Surfaces 3D {#mainpage}

Ce programme écrit en c++ permet de visualiser la discrétisation de courbes et de surfaces paramétriques.

## Courbes

Il est possible de visualiser deux types de courbe : les courbes de **Bézier** et les courbes **B-Spline**. Les courbes sont générées à partir d'une liste de points de contrôle. Par défaut, le programme génère 4 points de contrôle aléatoires dans l'espace 3D et leur courbe associée.

 > **Courbes de Bézier**
> Le programme permet d'afficher la discrétisation de courbes de Bézier à l'aide des polynômes de Berstein ou de l'algorithme de De Casteljau.

> **Courbes B-Spline**
> Le programme gère les courbes B-Spline uniformes. Il utilise donc un vecteur de noeuds uniforme.
> On peut jouer sur la multiplicité des noeuds pour forcer les extrémités de la courbe à coincider avec le polynôme de contrôle.
> Si de plus on ne fournit pas de noeuds internes (seulement des noeuds terminaux), on retrouve une courbe de Bézier.

De plus, il est possible de choisir entre l'utilisation d'une paramétrisation **linéaire** et une paramétrisation  **par avancée de front** pour discrétiser les courbes.

## Surfaces

Il est possible de visualiser deux types de surface : les surfaces de **Bézier** et les surfaces **B-Spline**. Les surfaces sont générées à partir d'une grille de points de contrôle. L'utilisateur peut fournir cette grille à partir d'un fichier *OBJ*. Il peut également générer une nouvelle grille qu'il peut exporter au format *OBJ*, pour l'éditer ensuite à l'aide du programme de son choix.

> **Surfaces de Bézier**
> Le programme permet d'afficher la discrétisation de surfaces de Bézier à l'aide des polynômes de Berstein.

> **Surfaces B-Spline**
> Le programme gère également les surfaces B-Spline uniformes. Il utilise deux vecteurs de noeuds uniformes. On peut également retomber sur une surface de Bézier en jouant sur la multiplicité des noeuds externes et en omettant les noeuds internes.

La discrétisation des surfaces se base sur une paramétrisation **linéaire**.

## Autres outils

Il est possible de vérifier que deux surfaces données au programme présentent une **continuité C0** en comparant la position des points de contrôle au niveau de leur extrémités (onglet *outils*).

## Dépendances

Le programme à été testé avec *QT 5.14.1* et le compilateur *g++ 9.3.0*.
Les packages doxygen et graphviz sont nécessaires pour générer la documentation.

## Documentation

Pour la générer, se positionner dans le répertoire de *build* et taper la commande *make doc* (*make open_doc* l'ouvrir automatiquement ensuite). Le fichier généré est disponible dans le répertoire de *build* ici : *html/index.xhtml*.

## Fichiers fournis

Afin de tester le programme, certains fichiers *OBJ* ont été fournis dans le sous-répertoire *Objets_test*.

## Ressources

https://web.mit.edu/hyperbook/Patrikalakis-Maekawa-Cho/
