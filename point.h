#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <array>
#include <QVector3D>

class Point
{
private:
    std::array<float, 3> coords;

public:
	Point();
    Point(float x, float y, float z);
    Point(const float*);

    float getN(size_t) const;
	float getX() const;
	float getY() const;
	float getZ() const ;
	void get(float *) const;

    void setN(size_t, float);
    void setX(float);
    void setY(float);
    void setZ(float);
	void set(const float *);

    QVector3D toQVector() const;

    bool operator== (const Point &) const;
    bool operator!= (const Point &) const;

    Point& operator+= (const Point &);
    Point& operator*= (float);

    Point operator+ (const Point&) const;
    Point operator* (float) const;

    float operator[](size_t) const;
    float& operator[](size_t);

	friend std::ostream& operator<<(std::ostream&, const Point&);
};

#endif // POINT_H
