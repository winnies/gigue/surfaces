#include "opencontrolpolyhedrondialog.h"
#include "ui_opencontrolpolyhedrondialog.h"

#include <QFileDialog>
#include <QMessageBox>

OpenControlPolyhedronDialog::OpenControlPolyhedronDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenControlPolyhedronDialog)
{
    ui->setupUi(this);

    connect(ui->open, &QPushButton::clicked, this, &OpenControlPolyhedronDialog::open);
    connect(ui->create, &QPushButton::clicked, this, &OpenControlPolyhedronDialog::create);
}

OpenControlPolyhedronDialog::~OpenControlPolyhedronDialog()
{
    delete ui;
}

const QStringList &OpenControlPolyhedronDialog::getFileNames() const
{
    return m_fileNames;
}

const QVector<Grid<Point> > &OpenControlPolyhedronDialog::getGrids() const
{
    return m_grids;
}

size_t OpenControlPolyhedronDialog::count() const
{
    return m_grids.size();
}

void OpenControlPolyhedronDialog::open()
{
    auto fileNames = QFileDialog::getOpenFileNames(this, tr("Ouvrir un polyèdre de contrôle"), "", tr("3D mesh files(*.obj)"));
    if (fileNames.isEmpty()) {
        return;
    }

    for (const auto& fileName : fileNames) {
        auto grid = Grid<Point>::readGrid(fileName.toStdString());
        if (grid.size() == 0) {
            QMessageBox::critical(this, tr("Lecture du polyèdre de contrôle"), tr("Une erreur est survenue lors de la lecture de %1").arg(fileName));
            continue;
        }

        m_fileNames << fileName;
        m_grids << grid;
    }

    if (count() == 0) {
        return;
    }

    accept();
}

void OpenControlPolyhedronDialog::create()
{
    auto grid = Grid<Point>::newLinear(ui->height->value(), ui->width->value(), 1.f, 1.f);

    if (ui->save->isChecked()) {
        auto fileName = QFileDialog::getSaveFileName(this, tr("Enregistrer le polyèdre de contrôle"), "", tr("3D mesh files(*.obj)"));
        if (fileName.isEmpty()) {
            return;
        }

        if (!grid.writeGrid(fileName.toStdString())) {
            QMessageBox::critical(this, tr("Impossible d'enregistrer"), "Une erreur est survenue."); // TODO revoir
            return;
        }

        m_fileNames << fileName;
    } else {
        m_fileNames << "";
    }

    m_grids << grid;
    accept();
}
