#R. Raffin
#M1 Informatique, Aix-Marseille Université
#.pro de l'application de départ du TP1

QT	+= core gui
CONFIG	+= c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = surfaces
TEMPLATE = app

LIBS += -lGLU



# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

unix:!macx {
    LIBS += -lglut -lGLU
    LIBS += -L$$PWD/OpenMesh/liblinux/ -lOpenMeshCore

    INCLUDEPATH += $$PWD/OpenMesh/inc/
    DEPENDPATH += $$PWD/OpenMesh/inc/
    DEPENDPATH += $$PWD/OpenMesh/liblinux/
}

macx: {
    INCLUDEPATH += $$PWD/OpenMesh/inc/
    LIBS += -L$$PWD/OpenMesh/libosx/ -lOpenMeshCore -lOpenMeshTools
}

SOURCES += \
    addobjectdialog.cpp \
    bezierberstein.cpp \
    beziercasteljau.cpp \
    bspline.cpp \
    carreaubezier.cpp \
    continuitydialog.cpp \
    curve.cpp \
    curveparams.cpp \
    discretisation.cpp \
    main.cpp \
    mainwindow.cpp \
    myopenglwidget.cpp \
    object.cpp \
    objectmodel.cpp \
    opencontrolpolyhedrondialog.cpp \
    parametrization.cpp \
    segment.cpp \
    point.cpp \
    surface.cpp \
    surfaceaggregate.cpp \
    surfaceparams.cpp \
    util.cpp

HEADERS += \
    addobjectdialog.h \
    bezierberstein.h \
    beziercasteljau.h \
    bspline.h \
    carreaubezier.h \
    continuitydialog.h \
    curve.h \
    curveparams.h \
    discretisation.h \
    grid.h \
    mainwindow.h \
    myopenglwidget.h \
    object.h \
    objectmodel.h \
    opencontrolpolyhedrondialog.h \
    parametrization.h \
    segment.h \
    point.h \
    surface.h \
    surfaceaggregate.h \
    surfaceparams.h \
    util.h

FORMS += \
    addobjectdialog.ui \
    continuitydialog.ui \
    curveparams.ui \
    mainwindow.ui \
    opencontrolpolyhedrondialog.ui \
    surfaceparams.ui

DISTFILES += \
    basic.fsh \
    basic.vsh

RESOURCES += \
    resources.qrc

dox.target = doc
dox.commands = $$QMAKE_COPY $$PWD/Doxyfile $$OUT_PWD/Doxyfile; \
               $$QMAKE_STREAM_EDITOR -i \'s|SOURCES_DIR|$$PWD|\' $$OUT_PWD/Doxyfile; \
               $$QMAKE_STREAM_EDITOR -i \'s|DESTINATION_DIR|$$OUT_PWD|\' $$OUT_PWD/Doxyfile; \
               doxygen $$OUT_PWD/Doxyfile;
dox.depends =
dox.CONFIG =

open_dox.target = open_doc
open_dox.commands = xdg-open $$OUT_PWD/html/index.xhtml
open_dox.depends = doc
open_dox.CONFIG =

QMAKE_EXTRA_TARGETS += dox open_dox
