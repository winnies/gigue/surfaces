#ifndef OPENCONTROLPOLYHEDRONDIALOG_H
#define OPENCONTROLPOLYHEDRONDIALOG_H

#include <QDialog>
#include "grid.h"
#include "point.h"

namespace Ui {
class OpenControlPolyhedronDialog;
}

/*!
 * \brief Boîte de dialogue permettant de choisir plusieurs polyèdres de contrôle.
 */
class OpenControlPolyhedronDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OpenControlPolyhedronDialog(QWidget *parent = nullptr);
    ~OpenControlPolyhedronDialog();

    /*!
     * \brief Renvoie le chemin des fichiers des polyèdres de contrôle choisis.
     *
     * Le chemin est vide pour les polyèdres de contrôle générés.
     *
     * \return le chemin des fichiers des polyèdres de contrôle choisis
     */
    const QStringList& getFileNames() const;

    /*!
     * \brief Renvoie les polyèdres de contrôle choisis.
     * \return les polyèdres de contrôle choisis
     */
    const QVector<Grid<Point>>& getGrids() const;

    /*!
     * \brief Renvoie le nombre de polyèdres de contrôle choisis.
     * \return le nombre de polyèdres de contrôle choisis
     */
    size_t count() const;

private:
    void open();
    void create();

private:
    Ui::OpenControlPolyhedronDialog *ui;

    QStringList m_fileNames;
    QVector<Grid<Point>> m_grids;
};

#endif // OPENCONTROLPOLYHEDRONDIALOG_H
