#include "point.h"

Point::Point()
    : coords{0.f, 0.f, 0.f}
{
}

Point::Point(float x, float y, float z)
    : coords{x, y, z}
{
}

Point::Point(const float * data)
    : coords{data[0], data[1], data[2]}
{
}

float Point::getX() const
{
    return coords[0];
}

float Point::getY() const
{
    return coords[1];
}

float Point::getZ() const
{
    return coords[2];
}

void Point::get(float * t) const
{
    for (size_t i = 0; i < coords.size(); ++i) {
		t[i] = coords[i];
    }
}

float Point::getN(size_t r) const
{
    return coords.at(r);
}

void Point::setN(size_t r, float v)
{
    coords.at(r) = v;
}

void Point::set(const float * t)
{
    for (size_t i = 0; i < coords.size(); ++i) {
        coords[i] = t[i];
    }
}

QVector3D Point::toQVector() const
{
    return {coords[0], coords[1], coords[2]};
}

void Point::setX(float v)
{
    coords[0] = v;
}
void Point::setY(float v)
{
    coords[1] = v;
}
void Point::setZ(float v)
{
    coords[2] = v;
}

bool Point::operator==(const Point &p) const {
    return getX() == p.getX()
            && getY() == p.getY()
            && getZ() == p.getZ();
}

bool Point::operator!=(const Point &p) const {
    return !(*this == p);
}

Point& Point::operator+=(const Point &p)
{
    for (size_t i = 0; i < coords.size(); ++i) {
        coords[i] += p.coords[i];
    }

    return *this;
}

Point& Point::operator*=(float f)
{
    for (size_t i = 0; i < coords.size(); ++i) {
        coords[i] *= f;
    }

    return *this;
}

Point Point::operator+(const Point &p) const
{
    return Point{*this} += p;
}

Point Point::operator*(float f) const
{
    return Point{*this} *= f;
}

float Point::operator[](size_t i) const
{
    return coords[i];
}

float &Point::operator[](size_t i)
{
    return coords[i];
}

std::ostream& operator<<(std::ostream& out, const Point& p)
{
	return out << "[ " << p.coords[0] << " " << p.coords[1] << " " << p.coords[2] << " ]"; // can access private member Y::data
}
