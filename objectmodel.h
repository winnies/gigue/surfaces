#ifndef OBJECTMODEL_H
#define OBJECTMODEL_H

#include <QAbstractListModel>
#include "object.h"

/*!
 * \brief Un modèle Qt d'objets.
 */
class ObjectModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ObjectModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    const QString &getName(int index) const;
    void setName(int index, const QString& name);

    const Object &get(int index) const;
    Object &get(int index);

    void insert(ObjectPtr object);
    void remove(int index);

private:
    std::vector<ObjectPtr> m_objects;
};

#endif // OBJECTMODEL_H
