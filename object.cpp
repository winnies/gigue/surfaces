#include "object.h"
#include <QWidget>

Object::Object(QObject *parent)
    : QObject(parent)
    , widget(nullptr)
    , m_isShown(true)
{
}

Object::~Object()
{
    if (widget) {
        delete widget.data();
    }
}

bool Object::isShown() const
{
    return m_isShown;
}

void Object::setIsShown(bool _isShown)
{
    if (m_isShown != _isShown) {
        m_isShown = _isShown;
        emit isShownChanged(m_isShown);
    }
}

CurveObject::CurveObject(QObject *parent)
    : Object(parent)
{
}

SurfaceObject::SurfaceObject(QObject *parent)
    : Object(parent)
{
}
