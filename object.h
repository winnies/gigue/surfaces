#ifndef OBJECT_H
#define OBJECT_H

#include <memory>
#include <QString>
#include <QPointer>
class QWidget;

#include "curve.h"
#include "surface.h"

/*!
 * \brief Représente un objet affichable dans l'application.
 */
class Object : public QObject
{
    Q_OBJECT

public:
    Object(QObject * parent = nullptr);
    virtual ~Object();

    /*!
     * \brief le nom de l'objet.
     */
    QString name;

    /*!
     * \brief le widget qui permet de configurer l'objet.
     */
    QPointer<QWidget> widget;

    /*!
     * \brief Indique si l'objet est affiché.
     * \return vrai si l'objet est affiché
     */
    bool isShown() const;

    /*!
     * \brief Change l'état de l'affichage de l'objet.
     * \param isShown vrai si l'objet doit être affiché
     */
    void setIsShown(bool isShown);

signals:
    void isShownChanged(bool isShown);

private:
    bool m_isShown;
};

using ObjectPtr = std::unique_ptr<Object>;

/*!
 * \brief Un objet possédant une courbe.
 */
class CurveObject : public Object {
    Q_OBJECT

public:
    CurveObject(QObject * parent = nullptr);

    std::unique_ptr<Curve> curve;
};

/*!
 * \brief Un objet possédant une surface.
 */
class SurfaceObject : public Object {
    Q_OBJECT

public:
    SurfaceObject(QObject * parent = nullptr);

    std::unique_ptr<Surface> surface;
};

#endif // OBJECT_H
