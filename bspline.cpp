#include "bspline.h"
#include "point.h"
#include "segment.h"

#include <iostream>

float spline(const std::vector<float>& ts, int i, int n, float t)
{
    if(n == 0) {
        if(ts[i] <= t && t < ts[i+1]) {
            return 1.f;
        } else if(t == 1 && ts[i+1] == 1) {
            return 1.f;
        } else {
            return 0.f;
        }
    } else {
        float temp1 = 0.f;
        float dem1 = ts.at(i+n) - ts.at(i);
        if(dem1 != 0.f) {
            temp1 = (t - ts.at(i)) / dem1 * spline(ts, i, n-1, t);
        }

        float temp2 = 0.f;
        float dem2 = ts.at(i+n+1) - ts.at(i+1);
        if(dem2 != 0.f) {
            temp2 = (ts.at(i+n+1) - t) / dem2 * spline(ts, i+1, n-1, t);
        }

        return temp1 + temp2;
    }
}

BSplineCurve::BSplineCurve():
    m_order(0)
{
}

BSplineCurve::BSplineCurve(const std::vector<Point> &controlPoints)
{
    set(controlPoints);
}

BSplineCurve::BSplineCurve(const std::vector<Point> &controlPoints, unsigned long order, SplineType type)
{
    set(controlPoints, order, type);
}

Point BSplineCurve::getValue(float t) const
{
    assert(isValid(t));
    Point result;

    for(size_t i = 0; i < m_controlPoints.size(); ++i) {
        result += m_controlPoints[i] * spline(tab_t, i, m_order, t);
    }

    return result;
}

float BSplineCurve::getStartP() const
{
    return tab_t[m_order];
}

float BSplineCurve::getEndP() const
{
    return tab_t[m_controlPoints.size()];
}

const std::vector<Point> &BSplineCurve::controlPoints() const
{
    return m_controlPoints;
}

void BSplineCurve::set(const std::vector<Point> &controlPoints)
{
    set(controlPoints, 2, SplineType::Uniform);
}

void BSplineCurve::set(const std::vector<Point> &controlPoints, unsigned long order, SplineType type)
{
    assert(controlPoints.size() >= 2);
    m_controlPoints = controlPoints;

    if(type == SplineType::Uniform) {
        assert(order < m_controlPoints.size());
        m_order = order;
        tab_t.clear();
        tab_t.reserve(m_controlPoints.size() + m_order + 1);
        for(size_t i = 0; i < m_controlPoints.size() + m_order + 1; ++i) {
            tab_t.push_back((float)i/(m_controlPoints.size() + m_order));
        }
    } else if(type == SplineType::Bezier) {
        m_order = m_controlPoints.size() - 1;
        tab_t.clear();
        tab_t.reserve(2 * (m_order+1));
        for(int i = 0; i <= m_order; i++) {
            tab_t.push_back(0);
        }
        for(int i = 0; i <= m_order; i++) {
            tab_t.push_back(1);
        }
    }
}

void BSplineCurve::setControlPoint(size_t u, const Point &point)
{
    m_controlPoints[u] = point;
}

BSplineSurface::BSplineSurface()
    : m_uOrder(0), m_vOrder(0), m_type(SplineType::Bezier)
{
}

BSplineSurface::BSplineSurface(const Grid<Point> &controlPoints)
{
    set(controlPoints, 0, 0, SplineType::Bezier);
}

BSplineSurface::BSplineSurface(const Grid<Point> &controlPoints, unsigned long uOrder, unsigned long vOrder, SplineType type) {
    set(controlPoints, uOrder, vOrder, type);
}

Point BSplineSurface::getValue(const Surface::Param &param) const
{
    assert(isValid(param));
    Point result;

    for (size_t i = 0; i < m_controlPoints.getLinesNb(); ++i) {
        float r = spline(m_uTs, i, m_uOrder, param.u);
        for (size_t j = 0; j < m_controlPoints.getColNb(); ++j) {
            result += m_controlPoints.get(i, j) * r * spline(m_vTs, j, m_vOrder, param.v);
        }
    }

    return result;
}

Surface::Param BSplineSurface::getStartP() const
{
    return {m_uTs[m_uOrder], m_vTs[m_vOrder]};
}

Surface::Param BSplineSurface::getEndP() const
{
    return {m_uTs[m_controlPoints.getLinesNb()], m_vTs[m_controlPoints.getColNb()]};
}

const Grid<Point> &BSplineSurface::controlPoints() const
{
    return m_controlPoints;
}

void BSplineSurface::set(const Grid<Point> &controlPoints) {
    set(controlPoints, m_uOrder, m_vOrder, m_type);
}

void BSplineSurface::set(const Grid<Point> &controlPoints, unsigned long uOrder, unsigned long vOrder, SplineType type)
{
    assert(controlPoints.size() > 0);

    if(type == SplineType::Uniform) {
        assert(uOrder < controlPoints.getLinesNb() && vOrder < controlPoints.getColNb());
        m_type = type;
        m_controlPoints = controlPoints;
        m_uOrder = uOrder;
        m_vOrder = vOrder;
        m_uTs.clear();
        m_uTs.reserve(m_controlPoints.getLinesNb() + m_uOrder + 1);
        for(size_t i = 0; i < m_controlPoints.getLinesNb() + m_uOrder + 1; ++i) {
            m_uTs.push_back((float)i/(m_controlPoints.getLinesNb() + m_uOrder));
        }
        m_vTs.clear();
        m_vTs.reserve(m_controlPoints.getColNb() + m_vOrder + 1);
        for(size_t i = 0; i < m_controlPoints.getColNb() + m_vOrder + 1; ++i) {
            m_vTs.push_back((float)i/(m_controlPoints.getColNb() + m_vOrder));
        }
    } else if(type == SplineType::Bezier) {
        m_type = type;
        m_controlPoints = controlPoints;
        m_uOrder = controlPoints.getLinesNb() - 1;
        m_vOrder = controlPoints.getColNb() - 1;
        m_uTs.clear();
        m_uTs.reserve((m_uOrder+1) * 2);
        for(int i = 0; i <= m_uOrder; i++) {
            m_uTs.push_back(0);
        }
        for(int i = 0; i <= m_uOrder; i++) {
            m_uTs.push_back(1);
        }
        m_vTs.clear();
        m_vTs.reserve((m_vOrder+1) * 2);
        for(int i = 0; i <= m_vOrder; i++) {
            m_vTs.push_back(0);
        }
        for(int i = 0; i <= m_vOrder; i++) {
            m_vTs.push_back(1);
        }
    }
}

void BSplineSurface::setControlPoint(size_t u, size_t v, const Point& point) {
    m_controlPoints.get(u, v) = point;
}
