#include "curveparams.h"
#include "ui_curveparams.h"

#include "discretisation.h"
#include "parametrization.h"

CurveParams::CurveParams(myOpenGLWidget *view, CurveObject *curve, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CurveParams),
    m_view(view),
    m_object(curve)
{
    ui->setupUi(this);
    ui->discretizeComboBox->setCurrentIndex(0);
    ui->discretizeParams->setCurrentIndex(0);

    connect(m_object, &CurveObject::isShownChanged, this, &CurveParams::updateView);
    connect(ui->controlCheckBox, &QCheckBox::clicked, this, &CurveParams::updateView);
    connect(ui->discretizeComboBox, &QComboBox::currentTextChanged, this, &CurveParams::updateDiscretization);
    connect(ui->discretizeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), ui->discretizeParams, &QStackedWidget::setCurrentIndex);
    connect(ui->nbDecoupesSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &CurveParams::updateDiscretization);
    connect(ui->pasDSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &CurveParams::updateDiscretization);
    connect(ui->seuilMaxDSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &CurveParams::updateDiscretization);
    connect(ui->seuilMinDSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &CurveParams::updateDiscretization);
    connect(ui->pointGroupBox, &QGroupBox::clicked, this, &CurveParams::updateView);
    connect(ui->pointDSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &CurveParams::updatePoint);
    connect(ui->pointSlider, &QSlider::valueChanged, [this](int value) {
        ui->pointDSpinBox->setValue((double)value / ui->pointSlider->maximum());
    });
    connect(ui->modifPoint, &QGroupBox::clicked, this, &CurveParams::updateControlPoints);
    connect(ui->modifPoint, &QGroupBox::clicked, this, &CurveParams::updateView);
    connect(ui->slider_u, &QSlider::valueChanged, ui->value_u, &QSpinBox::setValue);
    connect(ui->value_u, QOverload<int>::of(&QSpinBox::valueChanged), this, &CurveParams::changeControlPoint);
    connect(ui->value_modif_x, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &CurveParams::translateControlPoint);
    connect(ui->value_modif_y, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &CurveParams::translateControlPoint);
    connect(ui->value_modif_z, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &CurveParams::translateControlPoint);

    updateControlPoints();
    updateDiscretization();
    std::vector<QRgb> colors;
    colors.push_back(qRgb(255, 0, 0));
    m_pointDI = view->drawPoints({{0, 0, 0}}, colors);
    updatePoint();
    changeControlPoint();
    updateView();
}

CurveParams::~CurveParams()
{
    delete ui;

    m_view->remove(m_discretizationDI);
    m_view->remove(m_controlPointsDI);
    m_view->remove(m_pointDI);
    m_view->repaint();
}

void CurveParams::updateView()
{
    m_view->isShown(m_discretizationDI) = m_object->isShown();
    m_view->isShown(m_controlPointsDI) = m_object->isShown() && (ui->controlCheckBox->isChecked() || ui->modifPoint->isChecked());
    m_view->isShown(m_pointDI) = m_object->isShown() && ui->pointGroupBox->isChecked();
    m_view->repaint();
}

void CurveParams::updateControlPoints()
{
    const auto& points = m_object->curve->controlPoints();

    ui->slider_u->setMaximum(points.size() - 1);
    ui->value_u->setMaximum(points.size() - 1);

    const auto& coord = m_object->curve->controlPoints()[ui->value_u->value()];

    std::vector<QRgb> colors;

    if(ui->modifPoint->isChecked())
    {
        for (auto point : points) {
            if(point == coord)
            {
                colors.push_back(qRgb(255, 0, 0));
            }
            else
            {
                colors.push_back(qRgb(0, 0, 255));
            }
        }
    }
    else
    {
        colors.push_back(qRgb(0, 0, 255));
    }



    m_controlPointsDI = m_view->drawPoints(points, colors, m_controlPointsDI);
    m_view->drawMode(m_controlPointsDI) = DrawMode::Point | DrawMode::Line;
    m_view->repaint();
}

void CurveParams::updateDiscretization()
{
    std::vector<float> params;

    if(ui->discretizeComboBox->currentText() == "Découpe") {
        params = Parametrization::linear(*m_object->curve, ui->nbDecoupesSpinBox->value());
    } else if(ui->discretizeComboBox->currentText() == "Avancée de front") {
        params = Parametrization::avanceeDeFront(*m_object->curve, ui->pasDSpinBox->value(), ui->seuilMinDSpinBox->value(), ui->seuilMaxDSpinBox->value());
    }


    std::vector<QRgb> colors;
    colors.push_back(qRgb(190, 190, 190));

    m_discretizationDI = m_view->drawPoints(Discretize::discretize(*m_object->curve, params), colors, m_discretizationDI);
    m_view->drawMode(m_discretizationDI) = DrawMode::Point | DrawMode::Line;
    m_view->repaint();
}

void CurveParams::updatePoint()
{
    ui->pointSlider->setValue(ui->pointDSpinBox->value() * ui->pointSlider->maximum());

    auto start = m_object->curve->getStartP(), end = m_object->curve->getEndP();
    auto p = m_object->curve->getValue(start + (end - start) * ui->pointDSpinBox->value());
    auto& m = m_view->matrix(m_pointDI);
    m.setToIdentity();
    m.translate(p.toQVector());
    m_view->repaint();
}

void CurveParams::changeControlPoint()
{
    canTranslateControlPoint = false;

    ui->slider_u->setValue(ui->value_u->value());

    const auto& point = m_object->curve->controlPoints()[ui->value_u->value()];
    ui->value_modif_x->setValue(point.getX());
    ui->value_modif_y->setValue(point.getY());
    ui->value_modif_z->setValue(point.getZ());

    canTranslateControlPoint = true;

    updateControlPoints();
}

void CurveParams::translateControlPoint()
{
    if (!canTranslateControlPoint)  {
        return;
    }

    m_object->curve->setControlPoint(ui->value_u->value(), Point(ui->value_modif_x->value(), ui->value_modif_y->value(), ui->value_modif_z->value()));
    updateControlPoints();
    updateDiscretization();
    updatePoint();
}
