#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>
#include <iostream>

#include "myopenglwidget.h"
#include "discretisation.h"
#include "parametrization.h"
#include "beziercasteljau.h"

void myOpenGLWidget::Draw::tearGLObjects() {
    if (vao) {
        if (vao->isCreated()) {
            vao->destroy();
        }
        delete vao;
        vao = nullptr;
    }
    if (vaoFlat) {
        if (vaoFlat->isCreated()) {
            vaoFlat->destroy();
        }
        delete vaoFlat;
        vaoFlat = nullptr;
    }
    if (vboVertexArray.isCreated()) {
        vboVertexArray.destroy();
    }
    if (vboVertexArrayFlat.isCreated()) {
        vboVertexArrayFlat.destroy();
    }
    if (vboElementsLines.isCreated()) {
        vboElementsLines.destroy();
    }
    if (vboElementsTriangles.isCreated()) {
        vboElementsTriangles.destroy();
    }
}

static const QString vertexShaderFile   = ":/basic.vsh";
static const QString fragmentShaderFile = ":/basic.fsh";

myOpenGLWidget::myOpenGLWidget(QWidget *parent) :
    QOpenGLWidget(parent)
{
    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16); // antialiasing (x16)
    if (OPENGL_DEBUG) {
        sf.setOption(QSurfaceFormat::DebugContext);
    }
    setFormat(sf);

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus
}

myOpenGLWidget::~myOpenGLWidget()
{
    clear();
}

DrawId myOpenGLWidget::drawPoints(const std::vector<Point> &points, std::vector<QRgb> &colors, DrawId id)
{
    makeCurrent();

    auto* vao = new QOpenGLVertexArrayObject;
    vao->create();
    vao->bind();


     float r, g, b, a;
    QVector<GLfloat> data;
    data.reserve(points.size() * 7);

    for (size_t i = 0; i < points.size(); i++)
    {
        if(i >= colors.size())
        {
            r = qRed(colors.back()) / 255.f;
            g = qGreen(colors.back()) / 255.f;
            b = qBlue(colors.back()) / 255.f;
            a = qAlpha(colors.back());
        }
        else
        {
            r = qRed(colors[i]) / 255.f;
            g = qGreen(colors[i]) / 255.f;
            b = qBlue(colors[i]) / 255.f;
            a = qAlpha(colors[i]);
        }

        data << points[i].getX() << points[i].getY() << points[i].getZ()
             << r << g << b << a;
    }

    QOpenGLBuffer vbo;
    vbo.create();
    vbo.bind();
    vbo.allocate(data.constData(), data.count() * sizeof(GLfloat));

    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3, 7 * sizeof(GLfloat));
    m_program->setAttributeBuffer("colAttr", GL_FLOAT, 3 * sizeof(GLfloat), 4, 7 * sizeof(GLfloat));

    m_program->enableAttributeArray("posAttr");
    m_program->enableAttributeArray("colAttr");

    vao->release();

    static Draw defaultDraw;
    defaultDraw.mode = DrawMode::Point;

    Draw& draw = getOrNewDraw(id, defaultDraw);
    draw.tearGLObjects();
    draw.kind = DrawKind::Points;
    draw.vboVertexArray = vbo;
    draw.vao = vao;
    draw.vertexCount = points.size();

    doneCurrent();

    return id;
}

QMatrix4x4 &myOpenGLWidget::matrix(DrawId id)
{
    return m_draws.at(id).matrix;
}

const QMatrix4x4 &myOpenGLWidget::matrix(DrawId id) const
{
    return m_draws.at(id).matrix;
}

DrawMode &myOpenGLWidget::drawMode(DrawId id)
{
    return m_draws.at(id).mode;
}

DrawMode myOpenGLWidget::drawMode(DrawId id) const
{
    return m_draws.at(id).mode;
}

bool &myOpenGLWidget::isShown(DrawId id)
{
    return m_draws.at(id).isShown;
}

bool myOpenGLWidget::isShown(DrawId id) const
{
    return m_draws.at(id).isShown;
}

bool &myOpenGLWidget::ligthing(DrawId id)
{
    return m_draws.at(id).ligthing;
}

bool myOpenGLWidget::ligthing(DrawId id) const
{
    return m_draws.at(id).ligthing;
}

bool &myOpenGLWidget::flatLigthing(DrawId id)
{
    return m_draws.at(id).flatLighting;
}

bool myOpenGLWidget::flatLigthing(DrawId id) const
{
    return m_draws.at(id).flatLighting;
}

void myOpenGLWidget::remove(DrawId id)
{
    makeCurrent();
    m_draws.at(id).tearGLObjects();
    m_draws.erase(id);
    doneCurrent();
}

void myOpenGLWidget::messageLogged(QOpenGLDebugMessage message)
{
    qDebug() << message;
}

void myOpenGLWidget::initializeGL()
{
    initializeOpenGLFunctions();

    if (OPENGL_DEBUG) {
        QOpenGLDebugLogger* m_debugLogger = new QOpenGLDebugLogger(context());
        if (m_debugLogger->initialize()) {
            qDebug() << "GL_DEBUG Debug Logger" << m_debugLogger << "\n";
            connect(m_debugLogger, SIGNAL(messageLogged(QOpenGLDebugMessage)), this, SLOT(messageLogged(QOpenGLDebugMessage)));
            m_debugLogger->startLogging();
        }
    }

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glEnable(GL_DEPTH_TEST);

    m_modelView.setToIdentity();
    set_scene_pos(QVector3D(0.0, 0.0, 0.0), 1.0);

    //shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);

    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }
}

void myOpenGLWidget::resizeGL(int w, int h)
{
    update_projection_matrix();
    glViewport(0, 0, w, h);

    update();
}

void myOpenGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_program->bind(); // active le shader program
    m_program->setUniformValue("lightDir", QVector3D{0.f, 0.f, -1.f});

    QMatrix4x4 m = m_projection * m_modelView;

    glPointSize (5.0f);

    for (auto it : m_draws) {
        auto& draw = it.second;
        if (!draw.isShown) {
            continue;
        }

        auto matrix = m * draw.matrix;
        m_program->setUniformValue("matrix", matrix);
        m_program->setUniformValue("norMat", matrix.normalMatrix());
        m_program->setUniformValue("lighting", draw.ligthing);

        draw.vao->bind();

        switch (draw.kind) {
        case DrawKind::Points:
            if ((draw.mode & DrawMode::Point) == DrawMode::Point) {
               glDrawArrays(GL_POINTS, 0, draw.vertexCount);
            }
            if ((draw.mode & DrawMode::Line) == DrawMode::Line) {
                glDrawArrays(GL_LINE_STRIP, 0, draw.vertexCount);
            }
            break;
        case DrawKind::Mesh:
            if ((draw.mode & DrawMode::Point) == DrawMode::Point) {
                glDrawArrays(GL_POINTS, 0, draw.vertexCount);
            }
            if ((draw.mode & DrawMode::Line) == DrawMode::Line) {
                draw.vboElementsLines.bind();
                glDrawElements(GL_LINES, draw.elementsLinesCount, GL_UNSIGNED_INT, nullptr);
                draw.vboElementsLines.release();
            }
            if ((draw.mode & DrawMode::Triangle) == DrawMode::Triangle) {
                if (draw.flatLighting) {
                    draw.vaoFlat->bind();
                    glDrawArrays(GL_TRIANGLES, 0, draw.elementsTrianglesCount);
                    draw.vaoFlat->release();
                } else {
                    draw.vboElementsTriangles.bind();
                    glDrawElements(GL_TRIANGLES, draw.elementsTrianglesCount, GL_UNSIGNED_INT, nullptr);
                    draw.vboElementsTriangles.release();
                }
            }
            break;
        }

        draw.vao->release();
    }

    m_program->release();
}

void myOpenGLWidget::keyPressEvent(QKeyEvent *)
{
}

void myOpenGLWidget::keyReleaseEvent(QKeyEvent *)
{
}

void myOpenGLWidget::mousePressEvent(QMouseEvent *_event)
{
    last_point_ok_ = map_to_sphere(last_point_2D_ = _event->pos(), last_point_3D_);
}

void myOpenGLWidget::mouseReleaseEvent(QMouseEvent *)
{
    last_point_ok_ = false;
}

void myOpenGLWidget::mouseMoveEvent(QMouseEvent *_event)
{
    float* modelview_matrix_ = m_modelView.data();

    QPoint newPoint2D = _event->pos();

    QVector3D newPoint3D;
    bool newPoint_hitSphere = map_to_sphere(newPoint2D, newPoint3D);

    float dx = newPoint2D.x() - last_point_2D_.x();
    float dy = newPoint2D.y() - last_point_2D_.y();

    float w = width();
    float h = height();

    if ((_event->buttons() == (Qt::LeftButton + Qt::MidButton)) ||
        (_event->buttons() == Qt::LeftButton && _event->modifiers() == Qt::ControlModifier)) {
        float value_y = m_radius * dy * 3.f / h;
        translate(QVector3D(0.f, 0.f, value_y));
    } else if ((_event->buttons() == Qt::MidButton) ||
               (_event->buttons() == Qt::LeftButton && _event->modifiers() == Qt::AltModifier)) {
        float z = -(modelview_matrix_[2] * m_center[0] + modelview_matrix_[6] * m_center[1] +
                    modelview_matrix_[10] * m_center[2] + modelview_matrix_[14]) /
                  (modelview_matrix_[3] * m_center[0] + modelview_matrix_[7] * m_center[1] + modelview_matrix_[11] * m_center[2] +
                   modelview_matrix_[15]);
        float aspect     = w / h;
        float near_plane = 0.01f * m_radius;
        float top        = std::tan(45.f / 2.f * M_PI / 180.f) * near_plane;
        float right      = aspect * top;
        translate(QVector3D(2.f * dx / w * right / near_plane * z, -2.f * dy / h * top / near_plane * z, 0.0f));
    } else if (_event->buttons() == Qt::LeftButton) {
        if (last_point_ok_) {
            if ((newPoint_hitSphere = map_to_sphere(newPoint2D, newPoint3D))) {
                QVector3D axis = QVector3D::crossProduct(last_point_3D_, newPoint3D);
                if (axis.lengthSquared() < 1e-7f)
                    axis = QVector3D(1, 0, 0);
                else
                    axis.normalize();
                QVector3D d = last_point_3D_ - newPoint3D;
                float t     = 0.5f * d.length() / TRACKBALL_RADIUS;
                if (t < -1.f)
                    t = -1.f;
                else if (t > 1.f)
                    t = 1.0;
                float phi   = 2.f * std::asin(t);
                float angle = phi * 180.f / M_PI;
                rotate(axis, angle);
            }
        }
    }
    last_point_2D_ = newPoint2D;
    last_point_3D_ = newPoint3D;
    last_point_ok_ = newPoint_hitSphere;
    update();
}

void myOpenGLWidget::wheelEvent(QWheelEvent *_event)
{
    float d = -static_cast<float>(_event->delta()) / 120.f * 0.2f * m_radius;
    translate(QVector3D(0.0, 0.0, d));
    update();
    _event->accept();
}

void myOpenGLWidget::update_projection_matrix()
{
    m_projection.setToIdentity();
    m_projection.perspective(
                45.f, static_cast<float>(width()) / static_cast<float>(height()), 0.01f * m_radius, 100.f * m_radius);
}

void myOpenGLWidget::view_all()
{
    QVector3D translation = m_modelView * m_center;
    translation[2] += 3.f * m_radius;
    translation *= -1;
    translate(translation);
}

void myOpenGLWidget::set_scene_pos(const QVector3D &_cog, float _radius)
{
    m_center = _cog;
    m_radius = _radius;
    update_projection_matrix();
    view_all();
}

bool myOpenGLWidget::map_to_sphere(const QPoint &_point, QVector3D &_result)
{
    float x    = (2.f * _point.x() - width()) / width();
    float y    = -(2.f * _point.y() - height()) / height();
    float xval = x;
    float yval = y;
    float x2y2 = xval * xval + yval * yval;

    const float rsqr = TRACKBALL_RADIUS * TRACKBALL_RADIUS;
    _result[0]          = xval;
    _result[1]          = yval;
    if (x2y2 < 0.5f * rsqr)
        _result[2] = std::sqrt(rsqr - x2y2);
    else
        _result[2] = 0.5f * rsqr / std::sqrt(x2y2);

    return true;
}

void myOpenGLWidget::translate(const QVector3D &_trans)
{
    QMatrix4x4 translation;
    translation.translate(_trans);
    m_modelView = translation * m_modelView;
}

void myOpenGLWidget::rotate(const QVector3D &_axis, float _angle)
{
    QVector3D t = m_modelView * m_center;

    QMatrix4x4 rotation;
    rotation.setToIdentity();
    rotation.translate(t);
    rotation.rotate(_angle, _axis);
    rotation.translate(-t);

    m_modelView = rotation * m_modelView;
}

myOpenGLWidget::Draw &myOpenGLWidget::getOrNewDraw(DrawId& id, const Draw& defaultDraw)
{
    if (id == 0) {
        id = next_id++;
    }
    auto it = m_draws.find(id);
    if (it != m_draws.end()) {
       return it->second;
    } else {
        m_draws.insert({id, defaultDraw});
        return m_draws[id];
    }
}

void myOpenGLWidget::clear()
{
    makeCurrent();
    for (auto p : m_draws) {
        p.second.tearGLObjects();
    }
    m_draws.clear();
    doneCurrent();
}
