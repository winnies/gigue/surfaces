#ifndef SEGMENT_H
#define SEGMENT_H

#include "point.h"
#include "curve.h"

class Segment: public Curve
{
public:
    Segment() = default;
    Segment(const Point& start, const Point& end);

    Point getValue(float p) const;
    float getStartP() const;
    float getEndP() const;

    const std::vector<Point> &controlPoints() const;
    void set(const std::vector<Point> &controlPoints);
    void setControlPoint(size_t u, const Point& point);

    const Point& getN(size_t) const;
    const Point& getStart() const;
    const Point& getEnd() const;

    void setN(size_t, const Point&);
    void setStart(const Point&);
    void setEnd(const Point&);

	float length() const;

	friend std::ostream& operator<<(std::ostream&, const Segment&);

private:
    std::vector<Point> points;
    static constexpr float startP = 0.f, endP = 1.f;
};

#endif // SEGMENT_H
