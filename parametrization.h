#ifndef PARAMETRIZATION_H
#define PARAMETRIZATION_H

#include "point.h"
#include "grid.h"
#include "surface.h"
#include "curve.h"

/*!
 * \brief Méthodes de paramétrisation.
 * \sa Discretize
 */
namespace Parametrization {

/*!
 * \brief Renvoie une paramétrisation linéaire d'une courbe.
 * \param curve courbe paramétrique (fournit le domaine de paramétrisation)
 * \param count nombre de subdivisions
 * \return une paramétrisation linéaire sur le domaine de paramétrisation fourni par la courbe paramétrique
 */
std::vector<Curve::Param> linear(const Curve& curve, size_t count);

/*!
 * \brief Renvoie une paramétrisation linéaire d'une surface.
 * \param surface surface paramétrique (fournit le domaine de paramétrisation)
 * \param countU nombre de subdivisions sur l'axe u
 * \param countV nombre de subdivisions sur l'axe v
 * \return une paramétrisation linéaire sur le domaine de paramétrisation fourni par la surface paramétrique
 */
Grid<Surface::Param> linear(const Surface& surface, size_t countU, size_t countV);

/*!
 * \brief Renvoie une paramétrisation par avancée de front d'une courbe.
 * \param curve courbe paramétrique (fournit le domaine de paramétrisation)
 * \param step pas initial de la paramétrisation
 * \param minAngle angle minimum en dessous duquel le pas est augmenté (en radian)
 * \param maxAngle angle maximum au delà duquel le pas est diminué (en radian)
 * \return une paramétrisation par avancée de front sur le domaine de paramétrisation fourni par la courbe paramétrique
 */
std::vector<Curve::Param> avanceeDeFront(const Curve& curve, float step, float minAngle, float maxAngle);

}

#endif // PARAMETRIZATION_H
