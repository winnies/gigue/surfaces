#ifndef CONTINUITYDIALOG_H
#define CONTINUITYDIALOG_H

#include <QDialog>
#include <QSortFilterProxyModel>

#include "objectmodel.h"

namespace Ui {
class ContinuityDialog;
}

/*!
 * \brief Boîte de dialogue permettant de tester la continuité C0 de deux surfaces.
 */
class ContinuityDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ContinuityDialog(ObjectModel* objectmodel, QWidget *parent = nullptr);
    ~ContinuityDialog();

private:
    void updateComboBoxes();
    void verifyContinuity();

private:
    Ui::ContinuityDialog *ui;
    ObjectModel* m_objectModel;
    QSortFilterProxyModel* m_proxy;
};

#endif // CONTINUITYDIALOG_H
