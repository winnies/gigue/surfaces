#ifndef UTIL_H
#define UTIL_H

#include <vector>

namespace Util
{

/*!
 * \brief Retourne les valeurs de la dernière ligne du triangle de Pascal de hauteur ordre + 1.
 * \param order nombre d'étages du triangle (première ligne exclue)
 * \return les valeurs de la dernière ligne du triangle de Pascal de hauteur ordre + 1
 */
std::vector<long long> pascalTriangle(size_t order);

};

#endif // UTIL_H
