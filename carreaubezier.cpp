#include <cmath>
#include "carreaubezier.h"
#include "bezierberstein.h"
#include "util.h"

CarreauBezier::CarreauBezier()
    : m_uOrder(0)
    , m_vOrder(0)
{
}

CarreauBezier::CarreauBezier(const Grid<Point>& controlPoints)
{
    set(controlPoints);
}

Point CarreauBezier::getValue(const Param& param) const
{
    assert(isValid(param));
    Point result;

    for(int i(0); i <= m_uOrder; i++) {
        float bin = m_uCoefs[i] * std::pow((1 - param.u), m_uOrder - i) * std::pow(param.u, i);
        for(int j(0); j <= m_vOrder; j++) {
            result = result + m_controlPoints.get(i, j)
                    * bin
                    * m_vCoefs[j] * std::pow((1 - param.v), m_vOrder - j) * std::pow(param.v, j);
        }
    }

    return result;
}

Surface::Param CarreauBezier::getStartP() const
{
    return {0.f, 0.f};
}

Surface::Param CarreauBezier::getEndP() const
{
    return {1.f, 1.f};
}

const Grid<Point> &CarreauBezier::controlPoints() const
{
    return m_controlPoints;
}

void CarreauBezier::set(const Grid<Point> &controlPoints)
{
    assert(controlPoints.size() >= 2);
    m_controlPoints = controlPoints;
    m_uOrder = controlPoints.getLinesNb() - 1;
    m_vOrder = controlPoints.getColNb() - 1;
    m_uCoefs = Util::pascalTriangle(m_uOrder);
    m_vCoefs = Util::pascalTriangle(m_vOrder);
}

void CarreauBezier::setControlPoint(size_t u, size_t v, const Point& point) {
    m_controlPoints.get(u, v) = point;
}
