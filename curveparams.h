#ifndef CURVEPARAMS_H
#define CURVEPARAMS_H

#include <QWidget>

#include "myopenglwidget.h"
#include "object.h"

namespace Ui {
class CurveParams;
}

/*!
 * \brief Widget permetant de configurer l'affichage d'une courbe.
 */
class CurveParams : public QWidget
{
    Q_OBJECT

public:
    explicit CurveParams(myOpenGLWidget* view, CurveObject* curve, QWidget *parent = nullptr);
    ~CurveParams();

private:
    void updateView();
    void updateControlPoints();
    void updateDiscretization();
    void updatePoint();
    void changeControlPoint();
    void translateControlPoint();

private:
    Ui::CurveParams *ui;
    myOpenGLWidget* m_view;
    CurveObject* m_object;

    DrawId m_discretizationDI = 0;
    DrawId m_controlPointsDI = 0;
    DrawId m_pointDI = 0;

    bool canTranslateControlPoint = true;
    DrawId test;
};

#endif // CURVEPARAMS_H
