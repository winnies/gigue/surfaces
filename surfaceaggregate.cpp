#include "surfaceaggregate.h"

bool SurfaceAggregate::isC0(const Surface& s1, const Surface& s2)
{
    enum LineKind { Row, Column };

    auto compareLines = [&](size_t line1, LineKind kind1, size_t line2, LineKind kind2) {
        auto size = [](const Grid<Point>& g, LineKind kind) {
            switch (kind) {
                case Row: return g.getColNb();
                case Column: return g.getLinesNb();
            }
        };
        size_t size1 = size(s1.controlPoints(), kind1), size2 = size(s2.controlPoints(), kind2);

        if (size1 != size2) {
            return false;
        }

        auto iterLine = [](const Grid<Point>& g, size_t line, LineKind kind) {
            return [&g, line, kind](size_t i) {
                switch (kind) {
                    case Row: return g.get(line, i);
                    case Column: return g.get(i, line);
                }
            };
        };
        auto iterLine1 = iterLine(s1.controlPoints(), line1, kind1), iterLine2 = iterLine(s2.controlPoints(), line2, kind2);

        bool equal = true;
        for (size_t i = 0; i < size1; ++i) {
            if (iterLine1(i) != iterLine2(i)) {
                equal = false;
                break;
            }
        }

        if (equal) {
            return true;
        }

        for (size_t i = 0; i < size1; ++i) {
            if (iterLine1(i) != iterLine2(size1 - i - 1)) {
                return false;
            }
        }

        return true;
    };

    bool result = false;

    auto s1lines = {std::make_pair(s1.controlPoints().getLinesNb(), LineKind::Row)
                , std::make_pair(s1.controlPoints().getColNb(), LineKind::Column)};
    auto s2lines = {std::make_pair(s2.controlPoints().getLinesNb(), LineKind::Row)
                , std::make_pair(s2.controlPoints().getColNb(), LineKind::Column)};

    for(auto line1 : s1lines) {
        for(auto line2 : s2lines) {
            if(line1.first == line2.first) {
                result |= compareLines(0, line1.second, 0, line2.second);
                result |= compareLines(0, line1.second, line2.first-1, line2.second);
                result |= compareLines(line1.first-1, line1.second, 0, line2.second);
                result |= compareLines(line1.first-1, line1.second, line2.first-1, line2.second);
            }
        }
    }

    return result;
}
