#ifndef SURFACEPARAMS_H
#define SURFACEPARAMS_H

#include <QWidget>
#include <QFileSystemWatcher>

#include "myopenglwidget.h"
#include "object.h"

namespace Ui {
class SurfaceParams;
}

/*!
 * \brief Widget permetant de configurer l'affichage d'une surface.
 */
class SurfaceParams : public QWidget
{
    Q_OBJECT

public:
    explicit SurfaceParams(myOpenGLWidget* view, SurfaceObject* object, QWidget *parent = nullptr);
    ~SurfaceParams();

    void setControlPointsFile(const QString& filename);

private:
    void updateView();
    void updateControlPoints();
    void updateDiscretization();
    void updatePoint();
    void reloadControlPoints(bool showMessage = true);
    void saveControlPoints();
    void watchControlPointsChanged(bool enabled);
    void controlPointsFileChanged();
    void updateDiscretDrawMethod();
    void changeControlPoint();
    void translateControlPoint();

private:
    Ui::SurfaceParams *ui;
    myOpenGLWidget* view;
    SurfaceObject* object;
    QString controlPointsFileName;
    QFileSystemWatcher* watcher;

    DrawId controlPointsDI = 0;
    DrawId discretizationDI = 0;
    DrawId pointDI = 0;

    bool canTranslateControlPoint = true;
};

#endif // SURFACEPARAMS_H
