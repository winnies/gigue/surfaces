#ifndef CURVE_H
#define CURVE_H

#include <vector>

class Point;

/*!
 * \brief Classe abstraite représentant les courbes paramétriques.
 */
class Curve
{
public:
    /*!
     * \brief Paramètre d'une courbe
     */
    using Param = float;

    virtual ~Curve() = default;

    /*!
     * \brief Retourne la valeur de la courbe paramétrique pour un paramètre t donné.
     * \param t paramètre de la courbe
     * \return la valeur de la courbe pour t donné
     */
    virtual Point getValue(Param t) const = 0;

    /*!
     * \brief Retourne la borne inférieure du domaine (intervalle des valeurs possibles) du paramètre.
     * \return la borne inférieure du domaine du paramètre
     */
    virtual Param getStartP() const = 0;

    /*!
     * \brief Retourne la borne supérieure du domaine (intervalle des valeurs possibles) du paramètre.
     * \return la borne supérieure du domaine du paramètre
     */
    virtual Param getEndP() const = 0;

    /*!
     * \brief Retourne l'ensemble des points de contrôle de la courbe représentant son polyèdre de contrôle.
     * \return l'ensemble des points de contrôle de la courbe
     */
    virtual const std::vector<Point> &controlPoints() const = 0;

    /*!
     * \brief Attribue l'ensemble des points de contrôle de la courbe représentant son polyèdre de contrôle.
     * \param controlPoints l'ensemble des points de contrôle de la courbe
     */
    virtual void set(const std::vector<Point> &controlPoints) = 0;

    /*!
     * \brief Modifie un point de contrôle de la courbe.
     * \param u le paramètre u
     * \param point le nouveau point
     */
    virtual void setControlPoint(size_t u, const Point& point) = 0;

    /*!
     * \brief Vérifie si le paramètre est bien défini dans le domaine.
     * \param param paramètre à vérifier
     * \return vrai si le paramètre est compris entre la borne inférieure et la borne supérieure
     */
    bool isValid(Param param) const;
};

#endif // CURVE_H
